# API Consumer interface

## Consuming topics

var consumer = new KafkaConsumer({
    groupId: "kafkaConsumer",
    clientId: "kafkaConsumer",
    brokerList: ["localhost:9092"],
    //statLogIntervalMs: 10000,
    //maxQueueSize: 2000,
    //maxWaitMs: 100,
    //winstonLogger: logger,
    //maxMessagesPerSecond: null,
    //stageTopicLoading: true,
    topicConfigs: [
        { topic: "entity.vehicle.position", handler: handler, handlerCallback: true, decoderType: "json", commitMode: "none", commitParameter: 1000, startFromLatest: true, disconnectAfterLoaded: false },
        //{ topic: "STAGE.entity.actualcall.key.v2", handler: handler, handlerCallback: true, decoderType: "json", commitMode: "none", commitParameter: 1000, startFromLatest: false },
        //{ topic: "STAGE.entity.actualcall.status", handler: handler, handlerCallback: true, decoderType: "json", commitMode: "none", commitParameter: 1000, startFromLatest: false },
        //{ topic: "STAGE.entity.actualcall.estimatedtime", handler: handler, handlerCallback: true, decoderType: "json", commitMode: "none", commitParameter: 10000, startFromLatest: false },
        //{ topic: "STAGE.entity.pass.key", handler: handler, handlerCallback: true, decoderType: "json", commitMode: "none", commitParameter: 1000, startFromLatest: false },
    ]
});

