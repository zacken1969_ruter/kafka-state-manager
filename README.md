# Kafka State manager
This package wraps the packages [node-rdkafka](https://www.npmjs.com/package/node-rdkafka) and [avro-schema-registry](https://www.npmjs.com/package/avro-schema-registry) to create a high level, high preforming NPM Package to source state from and persist state to kafka brokers. 

## Features
### High level consumer:
- Automatic back preasure tuning
- Time and message count based commits
- Start from last commit, earliest or latest message
- Skip decoding of messages older than age limit.
- Skip processing of messages based on user defined filter function.
- Automatic or set decoding of Avro, JSON, text and binary messages.
- Integration with Schema Registry for automatic lookup of Avro schemas.
- Notification when topics are loaded to current offset.
- Message consumtion speed trottling.
- Performance monitoring.

### High level producer:
- Automatic encoding of Avro and JSON messages.
- Integration with Schema Registry for automatic lookup of Avro schemas.

### State Manager
- Maintains one or more memory based states.
- Merge topic message content into to state(s).
- Persist a state to a topic.
- Automatic re-loading of state on start.
- Notification when state is ready.
- Notification when state changes.
- Uses Highlevel consumer and producer for transport.
- Merge, set and delete records of persisted state.
- Automaticaly remove state record when recieving a tombstone from a compacted topic.
- Periodic cleaning of state using user defined function, including tombstoning of persisted state.

### Admin module
- Create, change and delete topics on broker
- Create, change and delete schemas in schema registry

## Install

## Documentation

### Producer

### Consumer

### State manager

### Admin
*Contact henrik.forli@ruter.no for more information.*