// This class sends messages without a key to kafka topics

const EventEmitter = require('events');
const Kafka = require('node-rdkafka');
const { v4: uuid } = require('uuid');
const sharedFunctions = require("./shared.js");
const env = process.env.NODE_ENV || 'development';


class kafkaAdmin extends EventEmitter {
    constructor(adminConfig) {
        super(); //must call super for "this" to be defined.

        this.logger = sharedFunctions.getLogger(null, "KafkaAdmin>" + adminConfig.clientId);
        this.logger.info('KafkaAdmin class initializing');

        if (!adminConfig.clientId || adminConfig.clientId == "") {
            let clientId = uuid();
            adminConfig.clientId = clientId;
            this.logger.warn('ClientId is empty using generated id: ' + clientId);
        }
        if (!Array.isArray(adminConfig.brokerList) && (!adminConfig.brokerList || adminConfig.brokerList == "")) {
            adminConfig.brokerList = ["localhost:9092"];
            this.logger.warn('BrokerList is empty using localhost:9092');
        }
        if (!Array.isArray(adminConfig.brokerList)) adminConfig.brokerList = [adminConfig.brokerList];


        this.state = {
            isConnected: false
        }

        this.config = {
            adminConfig: adminConfig,
            nodeRDKafkaConfig: {
                'client.id': adminConfig.clientId,
                'metadata.broker.list': adminConfig.brokerList
            }
        }

        this.adminClient = Kafka.AdminClient.create(this.config.nodeRDKafkaConfig);

        const self = this;
        function shutdownConsumer() {
            self.disconnect();
        };
        process.on('SIGINT', shutdownConsumer);

    }

    async connect() {
        await new Promise((resolve, reject) => {
            if (!this.state.isConnected) {
                this.state.isConnected = true;
                this.emit('admin.connected', true);
                this.logger.info('Connected to brokers: ' + JSON.stringify(this.config.adminConfig.brokerList));
                resolve();
            } else {
                this.logger.warn("All ready connected from Kafka broker(s)");
                resolve();
            }
        });
    }

    async disconnect() {
        await new Promise((resolve, reject) => {
            if (this.state.isConnected) {
                this.adminClient.disconnect()
                this.state.isConnected = false;
                this.emit('admin.disconnected', true);
                this.logger.info("Disconnected from Kafka broker(s)");
                resolve();
            } else {
                this.logger.warn("All ready disconnected from Kafka broker(s)");
                resolve();
            }
        });
    }

    async createTopic(topic, numberOfpartitions, replicationFactor, config) {
        await new Promise((resolve, reject) => {
            //logger.debug("Function createTopic() called");

            var topicConfig = {
                topic: topic,
                num_partitions: numberOfpartitions || 1,
                replication_factor: replicationFactor || 1,
                config: config || {}
            }
            this.logger.debug("Creating topic: " + topic);
            this.adminClient.createTopic(topicConfig, 10000, (err) => {
                if (err) {
                    if (err.code == 36) {
                        this.logger.warn("Topic all ready exists. Skipping creation: " + topic);
                        resolve(topic);
                    } else {
                        this.logger.error(err);
                        reject(err);
                    }
                } else {


                    setTimeout(() => {
                        this.logger.info("Topic created: " + topic);
                        resolve(topic);
                    }, 1000);
                }
            });
        });
    }

    async deleteTopic(topic) {
        //this.logger.debug("Function deleteTopic() called");
        await new Promise((resolve, reject) => {
            this.adminClient.deleteTopic(topic, 10000, (err) => {
                if (err) {
                    if (err.code == 3) {
                        this.logger.warn("Topic does not exists. Skipping delete: " + topic);
                        resolve(topic);
                    } else {
                        this.logger.error(err);
                        reject(err);
                    }
                } else {
                setTimeout(() => {
                    this.logger.info("Topic deleted: " + topic);
                    resolve(topic);
                }, 1000);
            }
            });
        });
    }

    async createPartitions(topic, numberOfpartitions) {
        //this.logger.debug("Function createPartitions() called");
        await new Promise((resolve, reject) => {
            this.adminClient.createPartitions(topic, numberOfpartitions, 10000, (err) => {
                if (err) {
                    if (err.code == 3) {
                        this.logger.warn("Topic does not exists. Skipping partition create: " + topic);
                        resolve(topic);
                    } else {
                        this.logger.error(err);
                        reject(err);
                    }
                } else {
                this.logger.info("Increased topic partitions: " + topic);
                resolve(topic);
                }
            });
        });
    }

    topicExists(topic) {
        logger.debug("Function topicExists() called");
        return new Promise((resolve, reject) => {


        });
    }

    getTopics() {
        logger.debug("Function getTopics() called");
        return new Promise((resolve, reject) => {


        });
    }
}

function _updateBokerTopics(context, topics) {
    context.state.brokerTopics.clear();
    if (Array.isArray(topics)) {
        topics.forEach(topic => {
            context.state.brokerTopics.set(topic.name, topic);
        })
    }
}


module.exports = kafkaAdmin;