// This class sends messages without a key to kafka topics

const EventEmitter = require('events');
const Kafka = require('node-rdkafka');
const async = require("async");
//const uuid = require("uuid/v4");
const { v4: uuid } = require('uuid');
const sharedFunctions = require("./shared.js");
const env = process.env.NODE_ENV || 'development';


class kafkaProducer extends EventEmitter {
    constructor(producerConfig) {
        super(); //must call super for "this" to be defined.

        this.logger = sharedFunctions.getLogger(null, "KafkaProducer>" + producerConfig.clientId);
        this.logger.info('Producer initializing');


        producerConfig.compression=producerConfig.compression || "none";
        producerConfig.clientId = producerConfig.clientId || uuid();
        producerConfig.maxWaitMs = producerConfig.maxWaitMs || 100;
        producerConfig.queueSize = producerConfig.queueSize || 2000;
        producerConfig.batchSize = producerConfig.batchSize || 2000;
        producerConfig.schemaRegistryUrl = producerConfig.schemaRegistryUrl || null;
        producerConfig.brokerList = producerConfig.brokerList || ["localhost:9092"];
        if (!Array.isArray(producerConfig.brokerList)) producerConfig.brokerList = [producerConfig.brokerList];

        this.config = {
            producerConfig: producerConfig
        };

        //Create consumer and producer configs
        this.config.nodeRDKafkaConfig = {
            'client.id': producerConfig.clientId,
            'message.max.bytes':15728640,
            'metadata.broker.list': producerConfig.brokerList,
            'max.in.flight.requests.per.connection': 1,
            'retry.backoff.ms': 2,
            'compression.codec':producerConfig.compression,
            'message.send.max.retries': 5,
            'socket.keepalive.enable': true,
            'queue.buffering.max.messages': producerConfig.queueSize,
            'queue.buffering.max.ms': producerConfig.maxWaitMs,
            'batch.num.messages': producerConfig.batchSize,
            'dr_cb': true
        };
        //this._broker = kafkaConfig.broker;
        this.state = {
            isConnected: false,
            brokerTopics: new Map(),
            inQueue: 0
        }

        this.encoder = null;
        if (producerConfig.schemaRegistryUrl) {
            this.logger.info('Producer has schema registry: ' + producerConfig.schemaRegistryUrl);
            const AvroCoder = require("./avroCoder");
            this.encoder = new AvroCoder(producerConfig.schemaRegistryUrl);
            //this.encoder = require('avro-schema-registry')(producerConfig.schemaRegistryUrl);
        }

        //Setup kafka Producer
        this.producer = new Kafka.Producer(this.config.nodeRDKafkaConfig, { 'request.required.acks': 1 });
        const self = this
        this.producer.on('delivery-report', function (err, report) {
            //console.log("DR");
            if (err) self.logger.error(err);
            else self.state.inQueue--;
        });

        //gracefull shutdown of consumer
        function shutdownConsumer() {
            self.disconnect();
        };
        process.on('SIGINT', shutdownConsumer);
    }

    async connect() {
        await new Promise((resolve, reject) => {
            this.producer.connect((err) => {
                if (err) reject();
            });
            let self = this;
            this.producer.once('ready', () => {
                this.producer.getMetadata({}, (error, rawBrokerTopics) => {
                    rawBrokerTopics.topics.forEach(topic => {
                        this.state.brokerTopics.set(topic.name, topic.partitions.length);
                        //this.logger.silly('Recieved valid valid topic from broker: ' + topic.name + " (" + topic.partitions.length + ")");
                    })
                    this.producer.setPollInterval(10);

                    this.state.isConnected = true;
                    this.logger.info('Connected to brokers: ' + JSON.stringify(this.config.producerConfig.brokerList));
                    this.emit('producer.connected');

                    resolve();
                });
            });
        })
    }


    async disconnect() {
        await new Promise((resolve, reject) => {
            if (this.pollTimer) {
                clearInterval(this.pollTimer);
                this.pollTimer == null;
            }

            async.whilst(
                (done) => {
                    done(null, this.state.inQueue > 0);
                },
                (done) => {
                    setTimeout(() => { done(null); }, 10);
                },
                (err) => {
                    if (err) reject(err);
                    else {
                        this.producer.disconnect((err) => {
                            if (err) reject();
                        });
                        this.producer.once('disconnected', () => {
                            this.state.brokerTopics = new Map();
                            this.state.isConnected = false;
                            this.logger.info('Disconnected from Kafka broker(s)');
                            this.emit('producer.disconnected');
                            resolve();
                        });
                    }
                });

        });
    }

    async send(topic, message, key, schema) {
        let avroEncoded = false;
        if (schema) {
            if(schema===true) message = await this.encoder.encode(message, topic);
            else message = await this.encoder.encode(message, topic, schema);
            avroEncoded = true;
        }

        await new Promise((resolve, reject) => {
            if (!message || (message == "null") || (message == "undefined")) message = "";
            if (this.state.isConnected) {

                if (this.state.inQueue <= this.config.producerConfig.queueSize) {
                    //if(schema)
                    if (!avroEncoded) message = Buffer.from(message);
                    this.producer.produce(topic, null, message, Buffer.from(key));
                    this.state.inQueue++;
                    resolve();

                }
                else {
                    let flushed = false;
                    async.whilst(
                        (done) => {
                            done(null, this.state.inQueue >= this.config.producerConfig.queueSize);
                        },
                        (done) => {
                            if (!flushed) {
                                flushed = true;
                                //this.producer.flush(5000);
                            }
                            setTimeout(() => { done(null); }, 10);
                        },
                        (err) => {
                            if (err) reject(err);
                            else {
                                if (!avroEncoded) message = Buffer.from(message);
                                this.producer.produce(topic, null, message, Buffer.from(key));
                                this.state.inQueue++;
                                resolve();
                            }
                        });
                }
            } else reject();
        });
    }

}


module.exports = kafkaProducer;