const sharedFunctions = require("./shared.js");

/*var registry = null;

var schemaRegistryUrl = null;



const MessageDecoderX = {
    decodeMessage: decodeMessage,
    schemaRegistryUrl: schemaRegistryUrl
}*/

class MessageDecoder {
    constructor(schemaRegistryUrl) {
        this.schemaRegistryUrl = schemaRegistryUrl;
        if (schemaRegistryUrl) {
            const AvroCoder = new require("./avroCoder.js");
            this.registry = new AvroCoder(schemaRegistryUrl);
        }
        //this.registry = require('avro-schema-registry')(schemaRegistryUrl);
    }

    //!!!This function is has side-effects changing the input object data to avoid unnessesary cloning giving lower performance
    async decodeMessage(data, decoderType) {
        //console.log(JSON.stringify(data.value))

        await new Promise((resolve, reject) => {
            let registry = this.registry;

            if (data.key) data.key = data.key.toString();
            if (data.value.length == 0) data.value = null;

            if (data.value) {
                if (decoderType == "AUTO") {
                    data.decodeMode = "AUTO";
                    if (data.value.length != null) {
                        if (data.value.readUInt8(0) !== 0) {
                            data.value = data.value.toString();
                            // Convert string to javascript object if JSON
                            data.value = sharedFunctions.convertIfJSON(data.value);
                            if ((typeof data.value === 'object') && (data.value !== null)) data.decodedType = "JSON";
                            else data.decodedType = "STRING";
                            resolve();
                        } else {
                            if (registry) {
                                // Decode Avro message
                                registry.decode(data.value).then(result => {
                                    data.value = result;
                                    data.decodedType = "AVRO";
                                    resolve();
                                }).catch(err => {
                                    reject(err);
                                });

                            }
                            else reject(new Error("No registry set up"));
                        }
                    }
                } else {
                    data.decodeMode = "FIXED";
                    if (decoderType == "JSON") {
                        try {
                            data.value = data.value.toString();
                            // Convert string to javascript object if JSON
                            data.value = sharedFunctions.convertIfJSON(data.value);
                            data.decodedType = "JSON";
                            resolve();
                        } catch (e) {
                            reject(new Error("Massage is not in JSON."));
                        }
                    } else if (decoderType == "AVRO") {
                        //console.log(JSON.stringify(data.value))
                        registry.decode(data.value).then(result => {
                            data.value = result;
                            data.decodedType = "AVRO";
                            resolve();
                        }).catch(err => {
                            reject(err);
                        });
                    } else if (decoderType == "STRING") {
                        data.value = data.value.toString();
                        data.decodedType = "STRING";
                        resolve();
                    } else if (decoderType == "BUFFER") {
                        //data.value = data.value;
                        data.decodedType = "BUFFER";
                        resolve();
                    }
                }
            } else resolve();
        });
        return data;
    }
}

module.exports = MessageDecoder;