// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');

const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");
var clone = require('clone');


class kafkaStateProducerV2 extends EventEmitter {
    constructor(kafkaConfig) {
        super(); //must call super for "this" to be defined.
        //Create consumer and producer configs


        //Setup topic maps for in-memory state
        this._topics = new Map();
        this._topicMap;
        this._loaded = false;

        this.config = kafkaConfig;

        var self = this;

        //console.log("In-memory structures ready");

        const { StateConsumerV2,ProducerV2 } = require("../index");

        // Override settings for state consuming
        this.config.stageTopicLoading=false;

        this.config.topicConfigs.forEach(topic => {
            topic.mapper = null;
            topic.commitMode="NONE";
            topic.startFromLatest= false;
            topic.maxAge = null;
            topic.disconnectAfterLoaded = true;
            
            this._topics.set(topic.topic, topic)
        })

        this.consumer = new StateConsumerV2(kafkaConfig);
        this._topicMap = this.consumer.getStateStore();
        console.log("StateProducer: Loading state");

        this.consumer.on("loaded", (topics) => {
            console.log("StateProducer: State Loaded. Activating producer");

            setupProducer();
        })

        this.consumer.on("message", (message) => {
            self.emit('update', message);
        });



        //Setup kafka Producer
        function setupProducer() {
            self._producer = new ProducerV2(kafkaConfig);
            self._producer.on("connected", () => {
                self._loaded = true;
                console.log("StateProducer: Ready to change state");
                self.emit('loaded', self._topicMap);
            })
        }
    }

    merge(topic, key, record) {
        var oldRecord = this._topicMap.get(topic).get(key);
        var newRecord = record;
        const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray
        if (oldRecord) newRecord = deepmerge(oldRecord.state, record,{ arrayMerge: overwriteMerge });
        this.set(topic, key, newRecord);
    }

    set(topic, key, record) {
        if (this._loaded) {
            var _topic=this._topicMap.get(topic)
            if(!_topic) {
                _topic=new Map();
                this._topicMap.set(topic,_topic);
            }
            var oldRecord = _topic.get(key);
            var changes = { dummy: 1 };
            if (oldRecord) {
                changes = diff(oldRecord.state, record);
            } ;
            if (Object.keys(changes).length != 0) {
                this._topicMap.get(topic).set(key, {stateCollection:topic,stateKey:key,state:record,updateTimestamp:new Date()});
                this._producer.send(topic, record, key);
            }
        } else throw new Error("StateProducer: Not ready to produce state")
    }

    delete(topic, key) {
        var record = this._topicMap.get(topic).get(key);
        if (record) {
            this._topicMap.get(topic).delete(key);
            this._producer.send(topic,null, key);
        }
    }

    deleteState(topic) {
        this._topicMap.get(topic).forEach((value, key) => {
            this.delete(topic, key);
        })
    }

    get(topic, key) {
        return clone(this._topicMap.get(topic).get(key));
    }

    getState(topic) {
        return this._topicMap.get(topic);
    }

    getStateStore() {
        return this._topicMap;
    }
}


module.exports = kafkaStateProducerV2;