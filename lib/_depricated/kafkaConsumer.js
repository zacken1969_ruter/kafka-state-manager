"use strict";
// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');
const kafka = require('node-rdkafka');
const async = require("async");

const uuid = require("uuid/v4");
const sharedFunctions = require("./shared.js");


const env = process.env.NODE_ENV || 'development';

const convertIfJSON = sharedFunctions.convertIfJSON;

var logger;


class KafkaConsumer extends EventEmitter {
    constructor(kafkaConfig) {
        super();
        var self = this;

        logger = sharedFunctions.getLogger(null, "KafkaConsumer");

        logger.info('Kafka consumer initializing');

        var groupId = kafkaConfig.groupId || uuid();
        logger.info('Using groupID: ' + groupId);
        var clientId = kafkaConfig.clientId || uuid();
        var maxWaitMs = kafkaConfig.maxWaitMs || 100;
        var maxMessagesPerSecond = kafkaConfig.maxMessagesPerSecond || null;
        var maxQueueSize = kafkaConfig.maxQueueSize || 1000;
        var schemaRegistryUrl = kafkaConfig.schemaRegistryUrl || null;
        var statLogIntervalMs = kafkaConfig.statLogIntervalMs || null;
        var topicConfigs = kafkaConfig.topicConfigs || [];
        var stageTopicLoading = kafkaConfig.stageTopicLoading || false;
        var brokerList = kafkaConfig.brokerList || ["localhost:9092"];
        if (!Array.isArray(brokerList)) brokerList = [brokerList];

        const consumerConfig = {
            'group.id': groupId,
            'metadata.broker.list': brokerList,
            'rebalance_cb': _onRebalance.bind(this),
            "client.id": clientId,
            "api.version.request": true,
            "auto.offset.reset": "beginning",
            "socket.keepalive.enable": true,
            "enable.auto.commit": false,
            "enable.auto.offset.store": false,
            "fetch.wait.max.ms": maxWaitMs,
            "broker.version.fallback": "0.10.1",
            "statistics.interval.ms": 1000,
            'offset_commit_cb': commitCallBack
        };



        logger.info('Connecting to kafka broker: ' + JSON.stringify(kafkaConfig.brokerList));
        this.consumer = new kafka.KafkaConsumer(consumerConfig, {
            "auto.offset.reset": "beginning"
        });
        this.connecting = false;
        this.connected = false;
        this.paused = false;
        this.loaded = false;
        this.assigned = false;
        //this.closed = false;

        const topics = new Map();

        this.topics = topics;
        this.subscribedTopics = [];
        this.remainingTopics = [];

        var registry = null;
        if (schemaRegistryUrl) {
            registry = require('avro-schema-registry')(schemaRegistryUrl);
            logger.info('Connecting to schema register: ' + schemaRegistryUrl);
        }
        this.schemaRegistryUrl = schemaRegistryUrl;

        this.brokerMetadata = null;
        this.topicMetadata = null;

        this.consumer.setDefaultConsumeTimeout(maxWaitMs);



        this.consumer.on('event.error', function (err) {
            logger.error(err);
            self.disconnect();
            //process.exit(1);

        });

        this.consumer.on('event.stats', function (stats) {
            logger.silly("Recieving broker stats")
            var data = JSON.parse(stats.message)
            this.topicMetadata = data;
            Object.keys(data.topics).forEach((topicKey) => {
                var topic = data.topics[topicKey];
                Object.keys(topic.partitions).forEach((partitionKey) => {
                    var partition = topic.partitions[partitionKey];
                    if (partition.partition >= 0) {
                        var topicRecord = topics.get(topic.topic);
                        var partitionRecord = topicRecord.assignments["partition" + partition.partition];
                        if (partitionRecord.status == "ASSIGNED") {
                            if (partition.consumer_lag > 0) {
                                partitionRecord.highOffset = partition.hi_offset - 1;
                                partitionRecord.offsetLag = partition.consumer_lag;
                            }
                            else
                                partitionRecord.offsetLag = 0;
                        }
                    }
                });
            });

            if (!self.loaded) {
                var removeTopics = [];
                topics.forEach((topicRecord) => {
                    if (!topicRecord.loaded) {
                        Object.keys(topicRecord.assignments).forEach(partition => {
                            var partitionRecord = topicRecord.assignments[partition];
                            if (partitionRecord.status == "ASSIGNED") {
                                if (!partitionRecord.loaded) {
                                    if (partitionRecord.offsetLag == 0) {
                                        partitionRecord.loaded = true;
                                        partitionRecord.loadedPrevious = true;
                                        partitionRecord.prevouslyLoadedOffset = partitionRecord.lastProcessedOffset;
                                        logger.debug("Topic " + topicRecord.topic + " (partition " + partitionRecord.partition + "), is loaded");
                                        self.emit('loaded.partition', topicRecord.topic, partitionRecord.partition);
                                    }
                                }
                            }
                        })

                        var loaded = true;
                        Object.keys(topicRecord.assignments).forEach(partition => {
                            var partitionRecord = topicRecord.assignments[partition];
                            if (!partitionRecord.loaded) loaded = false;
                        });
                        if (loaded) {
                            topicRecord.loaded = true;
                            if (!topicRecord.loadedPrevious) {
                                topicRecord.loadedPrevious = true;
                                if (stageTopicLoading && self.remainingTopics.length) {
                                    self.addSubscriptions([self.remainingTopics.shift()]);
                                }
                                logger.info("Topic " + topicRecord.topic + ", is loaded");
                                self.emit('loaded.topic', topicRecord.topic);
                                if (topicRecord.disconnectAfterLoaded) {
                                    removeTopics.push(topicRecord.topic);
                                }
                            }
                        }

                    }
                })
                if (removeTopics.length > 0) self.removeSubscriptions(removeTopics);
                var consumerLoaded = true;
                topics.forEach(topic => {
                    if (!topic.loaded) consumerLoaded = false;
                });
                if (consumerLoaded) {
                    logger.info("All current topics of consumer are loaded");
                    self.loaded = true
                    self.emit('loaded', topics);
                }

            }
        });

        self.connect(_initTopics);

        this.consumer.on('ready', () => {
            //_initTopics();
            self.emit('consumer.ready');
        });

        function _initTopics() {
            topicConfigs.forEach(topic => {
                var topicName = topic.topic;
                var brokerMetadata = self.brokerMetadata;
                if (topicName) {
                    var filter = topic.filter || null;
                    var handler = topic.handler || null;
                    var handlerCallback = topic.handlerCallback || false;
                    var disconnectAfterLoaded = topic.disconnectAfterLoaded || false;
                    var commitMode = topic.commitMode || "NONE";
                    var startFromLatest = topic.startFromLatest || false;
                    commitMode = commitMode.toUpperCase();
                    var commitParameter = topic.commitParameter || null;
                    if (commitMode == "NONE") commitParameter = null;
                    if (commitParameter == null) {
                        if (commitMode == "MAX_AGE") commitParameter = 1;
                        else if (commitMode == "TIME") commitParameter = 1;
                        else if (commitMode == "COUNT") commitParameter = 500;
                    }
                    var decoderType = topic.decoderType || "AUTO";
                    decoderType = decoderType.toUpperCase();
                    if ((decoderType == "AVRO") && !schemaRegistryUrl) throw new Error("Parameter schemaRegistryUrl must be set when using AVRO decoder.");
                    var maxAge = topic.maxAge || null;

                    self.subscribedTopics.push(topicName);
                    var topicRecord = {
                        topic: topicName,
                        status: "UNSUBSCRIBED",
                        filter: filter,
                        handler: handler,
                        handlerCallback: handlerCallback,
                        decoderType: decoderType,
                        commitMode: commitMode,
                        commitParameter: commitParameter,
                        maxAge: maxAge,
                        startFromLatest: startFromLatest,
                        assignments: {},
                        lastCommitTimestamp: null,
                        assignedPartitions: 0,
                        disconnectAfterLoaded: disconnectAfterLoaded,
                        uncommitedCount: 0,
                        processedCount: 0,
                        recievedCount: 0,
                        filteredCount: 0,
                        maxAgeDropCount: 0,
                        lastProcessedCount: 0,
                        lastRecievedCount: 0,
                        lastFilteredCount: 0,
                        lastMaxAgeDropCount: 0,
                        lastStatsTimestamp: null,
                        accumulatedProcessingTime: 0,
                        accumulatedProcessingCount: 0,
                    }

                    function _matchTopic(topic) {
                        return topic.name == topicName;
                    }
                    var brokerTopic = brokerMetadata.topics.find(_matchTopic);

                    brokerTopic.partitions.forEach(partition => {
                        topicRecord.assignments["partition" + partition.id] = {
                            partition: partition.id,
                            status: "UNASSIGNED",
                            loadedToCurrent: false,
                            lastRecievedTimestamp: null,
                            lastRecievedOffset: null,
                            lastProcessedOffset: null,
                            lastCommitedOffset: null,
                            highOffset: null,
                            lowOffset: null
                        }
                    });

                    //topicRecord.totalPartitions = brokerMetadata.topics.length;

                    topics.set(topicName, topicRecord);
                }

            });
            if (stageTopicLoading) {
                self.remainingTopics = self.subscribedTopics.slice(0);
                self.subscribedTopics = [self.subscribedTopics.shift()];
                self.remainingTopics.shift();
            } else self.remainingTopics = [];


            self.addSubscriptions(self.subscribedTopics);
            _consume();
        };

        var _started = false;

        this.consumer.on('data', (data) => {
            data.startTS = sharedFunctions.now('milli');
            if (!_started) {
                _started = true;
                self.emit('message.first');
                logger.info("Consumer recieving first data after connection");
            }
            if (self.connected && !self.disconnecting) {
                var topicRecord = topics.get(data.topic);
                var partitionRecord = topicRecord.assignments["partition" + data.partition];
                var watermarkOffsets = self.consumer.getWatermarkOffsets(data.topic, data.partition)
                var currentOffset = data.offset;
                topicRecord.recievedCount++;

                var skipFirst = false;
                if (!partitionRecord.lastRecievedOffset) skipFirst = true;
                partitionRecord.highOffset = watermarkOffsets.highOffset - 1;
                partitionRecord.lastRecievedOffset = currentOffset;
                partitionRecord.offsetLag = partitionRecord.highOffset - partitionRecord.lastRecievedOffset;
                partitionRecord.lastRecievedTimestamp = new Date(data.timestamp);
                if (!partitionRecord.lowOffset) partitionRecord.lowOffset = currentOffset;

                var _seekOK = false;
                if (partitionRecord.loadedPrevious && !partitionRecord.seekOffset) {
                    self.consumer.seek({ topic: data.topic, partition: data.partition, offset: partitionRecord.prevouslyLoadedOffset }, 0, () => { });
                    partitionRecord.seekOffset = partitionRecord.prevouslyLoadedOffset;
                    topics.set(data.topic, topicRecord);
                }
                if (topicRecord.startFromLatest && !partitionRecord.seekOffset) {
                    self.consumer.seek({ topic: data.topic, partition: data.partition, offset: watermarkOffsets.highOffset - 1 }, 0, () => { });
                    partitionRecord.seekOffset = watermarkOffsets.highOffset - 1;
                    topics.set(data.topic, topicRecord);
                }
                if (partitionRecord.seekOffset) {
                    if (partitionRecord.seekOffset <= data.offset) {
                        //delete partitionRecord.seekOffset;
                        _seekOK = true;
                        topics.set(data.topic, topicRecord);
                    }
                } else _seekOK = true;


                if (topicRecord && partitionRecord && _seekOK)
                    if (partitionRecord.status = "ASSIGNED") {
                        if (!partitionRecord.loadedToCurrent && (partitionRecord.offsetLag <= 0)) partitionRecord.loadedToCurrent = true;
                        topics.set(data.topic, topicRecord);


                        var cutoffTimestamp = new Date("1970-01-01");
                        if (topicRecord.maxAge) {
                            cutoffTimestamp = new Date() - (topicRecord.maxAge * 1000);
                        }
                        if (((data.timestamp > cutoffTimestamp) || (data.timestamp == -1)) && (!skipFirst)) self.msgQueue.push(data);
                        else {
                            if (!skipFirst) {
                                topicRecord.maxAgeDropCount++;
                                _maxAgeSkippedCounter++;
                                _backPressureSkipCounter++;
                            }
                        }

                    }
            }
        });

        var bufferSize = maxQueueSize;

        function _consume() {

            if (!self.paused) {

                var availableBuffer = (maxQueueSize - self.msgQueue.length()); //+ _backPressureSkipCounter;

                if (availableBuffer > maxQueueSize) {
                    availableBuffer = maxQueueSize;
                }
                _backPressureSkipCounter = 0;

                if (availableBuffer > 0) {
                    var start = new Date();
                    var _bufferSize = bufferSize;
                    if (_bufferSize > availableBuffer) _bufferSize = availableBuffer;
                    self.consumer.consume(_bufferSize, (err, msg) => {
                        if (err) logger.error(err);
                        else {
                            if (msg.length > 0) {
                                var diff = new Date() - start;
                                const hysteresis = 0.2;
                                var oldBufferSize = bufferSize;
                                if ((diff < (maxWaitMs - (maxWaitMs * hysteresis))) && (bufferSize < maxQueueSize)) {
                                    bufferSize = Math.round(bufferSize * 1.1);
                                    if (bufferSize == oldBufferSize) bufferSize++;
                                    if (bufferSize > maxQueueSize) bufferSize = maxQueueSize;
                                    logger.debug("Increased buffer size: " + oldBufferSize + ">" + bufferSize);
                                }
                                else if ((diff > (maxWaitMs + (maxWaitMs * hysteresis))) && (bufferSize > 1)) {
                                    bufferSize = Math.round(bufferSize * 0.9);
                                    if (bufferSize == oldBufferSize) bufferSize--;
                                    if (bufferSize < 1) bufferSize = 1;
                                    logger.debug("Decreased buffer size: " + oldBufferSize + ">" + bufferSize);
                                }

                            }

                        }
                        setImmediate(_consume);
                    });
                } else
                    setTimeout(_consume, maxWaitMs);
            } else setTimeout(_consume, maxWaitMs);
        }

        async function _onRebalance(err, assignments) {
            if (err.code === kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {

                self.consumer.assign(assignments);
                assignments.forEach(assignment => {
                    logger.debug("Assigning partitions");
                    var topicRecord = topics.get(assignment.topic);
                    var partitionRecord = topicRecord.assignments["partition" + assignment.partition];
                    if (topicRecord && partitionRecord) {
                        partitionRecord.status = "ASSIGNED";
                        topicRecord.loaded = false;
                        topicRecord.assignedPartitions++;
                        topics.set(assignment.topic, topicRecord);
                        logger.info("Consumer assignment added: " + assignment.topic + " partition " + assignment.partition);

                    }
                });
                self.loaded = false;
                self.assigned = true;

                self.emit('rebalance.assigned', assignments);
            } else if (err.code === kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {
                logger.debug("Revoking partitions");
                self.msgQueue.remove((d, p) => { return true; });
                assignments.forEach(assignment => {
                    var topicRecord = topics.get(assignment.topic);
                    var partitionRecord = topicRecord.assignments["partition" + assignment.partition];
                    if (topicRecord && partitionRecord) {
                        if ((topicRecord.commitMode != "NONE") && self.connected) {
                            if (partitionRecord.lastProcessedOffset != partitionRecord.lastCommitedOffset)
                                if (!self.consumer._isDisconnecting) self.consumer.commitSync({ topic: topicRecord.topic, partition: partitionRecord.partition, offset: partitionRecord.lastProcessedOffset });
                        }
                        if (topicRecord.commitMode == "TIME") {
                            clearInterval(topicRecord.commitTimer);
                        }
                        topicRecord.uncommitedCount = 0;
                        partitionRecord.status = "UNASSIGNED"
                        delete partitionRecord.seekOffset;
                        //delete topicRecord.assignments["partition" + assignment.partition];
                        //topicRecord.loaded = false;
                        topicRecord.assignedPartitions--;
                        topics.set(assignment.topic, topicRecord);
                        logger.debug("Consumer assignment removed: " + assignment.topic + " partition " + assignment.partition)
                    }
                });
                self.consumer.unassign();
                //self.loaded = false;
                self.assigned = false;

                self.emit('rebalance.revoked', assignments);
            } else {
                logger.error(err);
                self.emit('rebalance.error', err);
            }
        }

        var _processedCounter = 0;
        var _maxAgeSkippedCounter = 0;
        var _filterSkippedCounter = 0;
        var _backPressureSkipCounter = 0;
        var _lastStatTimestamp = new Date();

        const decoder = require("./decodeMessage");
        decoder.schemaRegistryUrl = this.schemaRegistryUrl;
        const decodeMessage = decoder.decodeMessage;


        var commitInProgress = false;
        function checkCommit(topicRecord) {
            topicRecord.uncommitedCount++;
            if (topicRecord.commitMode == "COUNT") {
                if (topicRecord.uncommitedCount >= topicRecord.commitParameter) {
                    self.commitTopic(topicRecord.topic);
                }
            } else if (topicRecord.commitMode == "TIME") {
                if (!topicRecord.commitTimer) {
                    topicRecord.commitTimer = setInterval(() => {
                        self.commitTopic(topicRecord.topic);
                    }, topicRecord.commitParameter);
                }
            }
        }

        function commitCallBack(err, topicPartitions) {
            if (err) {
                // There was an error committing
                logger.error(err);
            } else {
                logger.debug("Commit acknowledged")
                topicPartitions.forEach(partition => {
                    var topicRecord = topics.get(partition.topic);
                    if (topicRecord) {
                        topicRecord.assignments["partition" + partition.partition].lastCommitedOffset = partition.offset;
                    }
                });
            }
        }

        //var msgts = new Map();
        var maxMessagesPerSecondCounter = 0;
        var maxMessagesPerSecondCallBack = null;
        this.maxMessagesPerSecondTimer = null;
        if (maxMessagesPerSecond) {
            maxMessagesPerSecond = maxMessagesPerSecond / 5;
            this.maxMessagesPerSecondTimer = setInterval(() => {
                maxMessagesPerSecondCounter = 0;
                if (maxMessagesPerSecondCallBack) {
                    maxMessagesPerSecondCallBack();
                    maxMessagesPerSecondCallBack = null;
                }
            }, 200);
        }

        this.msgQueue = async.queue(async (data, queueCallBack) => {
            var topicRecord = topics.get(data.topic);

            var data = await decodeMessage(data, topicRecord.decoderType);
            //data = result;

            var process = true;
            if (topicRecord.filter) {
                if (!topicRecord.filter(data)) process = false;
            }
            function _handleCallback(topicRecord, processed) {
                if (processed) {
                    var endTS = sharedFunctions.now('milli');
                    var processingTimeUs = endTS - data.startTS;
                    topicRecord.accumulatedProcessingCount++;
                    topicRecord.accumulatedProcessingTime = topicRecord.accumulatedProcessingTime + processingTimeUs;
                    topicRecord.assignments["partition" + data.partition].lastProcessedOffset = data.offset;
                    topicRecord.assignments["partition" + data.partition].lastProcessedTimestamp = new Date(data.timestamp);
                    topicRecord.processedCount++;
                    _processedCounter++;
                }
                else topicRecord.filteredCount++;
                checkCommit(topicRecord);

                //EXTRACT TO FUNC?
                maxMessagesPerSecondCounter++;
                if ((maxMessagesPerSecondCounter >= maxMessagesPerSecond) && (maxMessagesPerSecond > 0)) {
                    maxMessagesPerSecondCallBack = queueCallBack;
                } else {
                    //maxMessagesPerSecondCallBack=null;
                    queueCallBack();
                }

            }
            if (process) {
                self.emit('message', data);
                if (topicRecord.handler) {
                    if (topicRecord.handlerCallback) {
                        function handlerCallBack() {
                            _handleCallback(topicRecord, true);
                        }
                        topicRecord.handler(data, handlerCallBack);
                    } else {
                        topicRecord.handler(data);
                        _handleCallback(topicRecord, true);

                    }
                } else {
                    _handleCallback(topicRecord, true);
                }
            } else {
                _handleCallback(topicRecord, false);
            }

        }, 1);

        if (statLogIntervalMs) {
            var _oldTotal = 0;
            setInterval(() => {
                if (self.connected && (self.consumer.subscription().length > 0)) {
                    var now = new Date();
                    var diff = (now - _lastStatTimestamp) / 1000;
                    _processedCounter = 0;
                    _maxAgeSkippedCounter = 0;
                    _filterSkippedCounter = 0;
                    _lastStatTimestamp = now;
                    var total = 0;
                    var accumulatedProcessingTime = 0;
                    var accumulatedProcessingCount = 0;
                    self.topics.forEach(topic => {
                        //logger.info(topic.topic + ": Offset-lag: " + offsetLag);

                        logger.silly(topic.topic + ": Recieved: " + topic.recievedCount + " Processed: " + topic.processedCount + " Filtered: " + topic.filteredCount + " maxAge Filtered: " + topic.maxAgeDropCount);
                        total = total + topic.processedCount;

                        var avgProcessingTime = topic.accumulatedProcessingTime / topic.accumulatedProcessingCount;
                        accumulatedProcessingTime = accumulatedProcessingTime + topic.accumulatedProcessingTime;
                        accumulatedProcessingCount = accumulatedProcessingCount + topic.accumulatedProcessingCount;
                        logger.silly(topic.topic + ": Average processing time: " + avgProcessingTime.toFixed(3) + " ms")
                    })
                    var avgProcessingTime = accumulatedProcessingTime / accumulatedProcessingCount;

                    logger.info("Stats: Speed: " + Math.round((total - _oldTotal) / diff) + " msg/s - Recieved: " + total + " Average processing time: " + avgProcessingTime.toFixed(3) + " ms");
                    _oldTotal = total;




                }
                logger.info("Memmory usage: " + (process.memoryUsage().rss / 1024 / 1024).toFixed(3) + " MB")
            }, statLogIntervalMs);
        }
    }

    addSubscriptions(topics = []) {
        var subscribedTo = this.consumer.subscription();
        if (subscribedTo && !!subscribedTo.length) {
            return this.adjustSubscription(subscribedTo.concat(topics));
        } else return this.adjustSubscription(topics);
    }

    removeSubscriptions(topics = []) {
        var subscribedTo = this.consumer.subscription();
        var newSubscription = [];
        if (subscribedTo) {
            subscribedTo.forEach(topic => {
                if (!topics.includes(topic)) newSubscription.push(topic);
            });
            return this.adjustSubscription(newSubscription);
        } else return [];
    }

    adjustSubscription(topics = []) {
        if (!Array.isArray(topics)) {
            topics = [topics];
        }
        if (!topics.length) {
            this.consumer.unsubscribe();
            this.topics = [];
            return [];
        } else {
            var subscribedTo = this.consumer.subscription();
            if (subscribedTo && !subscribedTo.length) {
                this.consumer.unsubscribe();
            }
            this.consumer.subscribe(topics);
            this.subscribedTopics = topics; //update member field
            return this.subscribedTopics;
        }
    }

    getBrokerTopics() {
        logger.debug("getBrokerTopics called");
        return this.brokerMetadata.topics;
    }

    pause() {
        logger.debug("pause called");
        this.msgQueue.pause();
        this.paused = true;
    }

    resume() {
        logger.debug("resume called");
        this.msgQueue.resume();
        this.paused = false;
    }

    commitAll(sync) {
        logger.debug("commitAll called");
        this.topics.forEach((topicRecord) => {
            this.commitTopic(topicRecord.topic, sync);
        })
    }

    commitTopic(topic, sync) {
        logger.debug("commitTopic called for topic " + topic + " Sync:" + sync);
        if (!this.disconnecting) {
            var topicRecord = this.topics.get(topic);
            if (topicRecord) {
                Object.keys(topicRecord.assignments).forEach(partition => {
                    var assignment = topicRecord.assignments[partition];
                    if (assignment.status == "ASSIGNED") {
                        if (assignment.lastProcessedOffset != assignment.lastCommitedOffset) {
                            logger.silly("Comitting " + topicRecord.topic + " partition " + assignment.partition + " Offset " + assignment.lastProcessedOffset)
                            if (sync) this.consumer.commitSync({ topic: topicRecord.topic, partition: assignment.partition, offset: assignment.lastProcessedOffset });
                            else this.consumer.commit({ topic: topicRecord.topic, partition: assignment.partition, offset: assignment.lastProcessedOffset });
                            //assignment.lastCommitedOffset = assignment.lastProcessedOffset;
                        }
                    }
                });
                topicRecord.uncommitedCount = 0;
            }
        }
    }



    disconnect() {
        logger.debug("disconnect called");
        var self = this;
        if (this.connected || this.connecting) {
            if (!this.disconnecting) {
                this.pause();
                logger.debug("Trying to disconnect from Kafka broker")
                this.topics.forEach((topicRecord) => {
                    if (topicRecord.commitMode != "NONE")
                        self.commitTopic(topicRecord.topic, true);
                })
                this.disconnecting = true;

                this.consumer.disconnect((err, data) => {
                    if (err) {
                        logger.error(err);
                        self.emit('disconnected.error', err);
                    }
                    this.brokerMetadata = null;
                    this.topicMetadata = null;
                    this.connecting = false;
                    this.connected = false;
                    this.disconnecting = false;
                    //this.closed = true;
                    logger.info("Disconnected from Kafka broker");
                    self.emit('disconnected', true);
                })
            }
        }
        return 0;
    }

    connect(callback) {
        logger.debug("connect called");
        var self = this;
        if (!this.connecting && !this.connected) {
            this.connecting = true;

            this.consumer.connect({}, (err, data) => {
                if (err) {
                    self.emit('connected.error', err);
                    this.connecting = false;
                    logger.info(err);
                }
                this.brokerMetadata = data;
                this.topicMetadata = null;
                this.resume();
                //this.closed = false;
                this.connected = true;
                this.connecting = false;
                logger.info("Connected to Kafka broker");
                self.emit('connected', true);
                if (callback) callback();
            });

        }
        return 0;
    }
}

module.exports = KafkaConsumer;