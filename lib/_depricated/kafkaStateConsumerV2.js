// This class maintains an in memory state that is populated from a kafka topic and published to the same topic when changed

const EventEmitter = require('events');
const diff = require("deep-object-diff").diff;
const async = require("async");

const deepmerge = require("deepmerge");
var clone = require('clone');



class kafkaStateConsumerV2 extends EventEmitter {
    constructor(kafkaConfig) {
        super();
        var self = this;
        this.loaded=false;
        this.config = kafkaConfig;
        this.states = new Map();
        this.topics = new Map();
        this.stateCleaner = kafkaConfig.stateCleaner||null;

        this.config.topicConfigs.forEach(topic => {
            topic.handler = mapperHandler;
            this.topics.set(topic.topic, topic)
        })

        console.log("StateConsumer: In-memory structures ready");

        const { ConsumerV2 } = require("../index");

        // Override settings for state consuming


        this.consumer = new ConsumerV2(kafkaConfig);

        this.consumer.on("loaded.topic", (topic) => {
            self.emit('loaded.topic', topic);
        })
        this.consumer.on("loaded", (topics) => {
            self.loaded=true;
            self.emit('loaded', topics);
            if(this.stateCleaner){
                function _clean(){
                    async.eachLimit(self.states,1, (state,stateCallback)=>{
                        //console.log("Cleaning state: "+ state[0]);
                        async.eachLimit(state[1],1, (message,callback)=>{
                            
                            if(!self.stateCleaner(message[1])){
                                //console.log("Removing state: "+ state[0]+"/"+message[0]);
                                self.states.get(state[0]).delete(message[0]);
                                self.emit('state.clean', message[1]);
                            }
                            setTimeout(callback,10);
                        },(err)=>{
                            console.log("Cleaning state done:" + state[0]);
                            setTimeout(stateCallback,1000);
                        })
    
                    },(err)=>{
                        //console.log("Cleaning done");
                        setTimeout(_clean,10000);
                    })
                }
                _clean();
            }
        })
        this.consumer.on("consumer.connected", () => {
            self.emit('consumer.connected');
        })
        this.consumer.on("rebalance.assigned", (assignments) => {
            self.emit('rebalance.assigned', assignments);
        })
        this.consumer.on("rebalance.revoked", (assignments) => {
            self.emit('rebalance.revoked', assignments);
        })
        this.consumer.on("rebalance.error", (err) => {
            self.emit('rebalance.error', err);
        })
        this.consumer.on("connection.disconnect.error", (err) => {
            self.emit('connection.disconnect.error', err);
        })
        this.consumer.on("connection.disconnect", () => {
            self.emit('connection.disconnect', true);
        })
        this.consumer.on("connection.connect.error", (err) => {
            self.emit('connection.connect.error', err);
        })
        this.consumer.on("connection.connect", () => {
            self.emit('connection.connect', true);
        })

        function mapperHandler(data) {
            var topicRecord = self.topics.get(data.topic);
            var message = {
                updateTimestamp: new Date(data.timestamp),
                stateKey: data.key,
                stateCollection: data.topic,
                merge: true,
                state: data.value
            }
            var mappedData;
            if (topicRecord.mapper) mappedData = topicRecord.mapper(clone(message));
            else mappedData = [message];
            if (!mappedData) mappedData = [];
            else if (!Array.isArray(mappedData)) mappedData = [mappedData];

            mappedData.forEach(record => {
                if(!record.state){
                    var state=self.states.get(record.stateCollection);
                    if (!state) {
                        self.states.set(record.stateCollection, new Map());
                        state = self.states.get(record.stateCollection);
                        console.log("Created state for " + record.stateCollection);
                    }
                    state.delete(record.stateKey);
                    self.emit('state.delete', record);
                }
                else if (record.merge) {
                    var stateRecord = merge(record);

                    update(stateRecord);
                } else {
                    update(record);
                }
            });
        }

        function merge(stateRecord) {
            var state = self.states.get(stateRecord.stateCollection);
            if (!state) {
                self.states.set(stateRecord.stateCollection, new Map());
                state = self.states.get(stateRecord.stateCollection);
                console.log("Created state for " + stateRecord.stateCollection);
            }
            var oldStateRecord = state.get(stateRecord.stateKey);
            if (oldStateRecord) {
                const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;
                stateRecord = deepmerge(oldStateRecord, stateRecord, { arrayMerge: overwriteMerge });
            }
            return stateRecord;
        }

        function update(stateRecord) {
            var state = self.states.get(stateRecord.stateCollection);
            if (!state) {
                self.states.set(stateRecord.stateCollection, new Map());
                state = self.states.get(stateRecord.stateCollection);
                console.log("Created state for " + stateRecord.stateCollection);
            }
            var oldRecord = state.get(stateRecord.stateKey);

            var changes = { dummy: 1 };
            if (oldRecord) {
                changes = diff(oldRecord.state, stateRecord.state);
            }
            if (Object.keys(changes).length != 0) {
                self.states.get(stateRecord.stateCollection).set(stateRecord.stateKey, stateRecord);
                self.emit('state.update', stateRecord);
            }
        }

        

    }

    close() {
        this.consumer.close();
    }

    get(topic, key) {
        return this.states.get(topic).get(key);
    }

    getState(topic) {
        return this.states.get(topic);
    }

    getStateStore() {
        return this.states;
    }
}


module.exports = kafkaStateConsumerV2;