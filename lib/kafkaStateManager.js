// This class sends messages without a key to kafka topics

const EventEmitter = require('events');
//const Kafka = require('./kafkaConnector.js');
//const uuid = require("uuid/v4");
const { v4: uuid } = require('uuid');
const sharedFunctions = require("./shared.js");
//const State = require('./state');
const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");
const env = process.env.NODE_ENV || 'development';


class kafkaStateManager extends EventEmitter {
    constructor(stateManagerConfig) {
        super(); //must call super for "this" to be defined.

        this.logger = sharedFunctions.getLogger(null, "KafkaStateManager>" + stateManagerConfig.clientId);
        this.logger.info('KafkaStateManager class initializing');

        if (!stateManagerConfig.clientId || stateManagerConfig.clientId == "") {
            let clientId = uuid();
            stateManagerConfig.clientId = clientId;
            this.logger.warn('ClientId is empty using generated id: ' + clientId);
        }
        if (!stateManagerConfig.groupId || stateManagerConfig.groupId == "") {
            let groupId = uuid();
            stateManagerConfig.groupId = groupId;
            this.logger.warn('GroupId is empty using generated id: ' + groupId);
        }
        if (!Array.isArray(stateManagerConfig.brokerList) && (!stateManagerConfig.brokerList || stateManagerConfig.brokerList == "")) {
            stateManagerConfig.brokerList = ["localhost:9092"];
            this.logger.warn('BrokerList is empty using localhost:9092');
        }
        if (!Array.isArray(stateManagerConfig.brokerList)) stateManagerConfig.brokerList = [stateManagerConfig.brokerList];

        this.state = {
            stateLogIntervalSec: stateManagerConfig.stateLogIntervalSec || null,
            stateTimer: null,
            stateLoaded: false,
            isConnected: false,
            states: new Map(),
            stateTopics: new Map(),
            consumerTopics: [],
        }

        this.config = {
            stateManagerConfig: stateManagerConfig,
            stateTopicConfigs: [],
            stateConfigs: new Map()
        }

        this.config.hasCleaners = false;

        //this.states = new Map();
        this.config.stateManagerConfig.states.forEach(stateConfig => {
            //let newState = new State(stateConfig.state);
            this.config.stateConfigs.set(stateConfig.stateName, stateConfig);
            this.state.states.set(stateConfig.stateName, new Map());
            if (stateConfig.stateTopic) {
                let stateTopicConfig = {
                    topic: stateConfig.stateTopic,
                    keepOffsetOnReassign: true,
                    priority: 0,
                    //maxAgeSec: 3600,
                    //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
                    handler: (message, done) => {
                        _stateHandler(this, stateConfig.stateName, message, done);
                    },
                    processingType: "CALLBACK",
                    startFromLatest: false,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                }
                this.config.stateTopicConfigs.push(stateTopicConfig);
                this.state.stateTopics.set(stateConfig.stateTopic, stateConfig.stateName);
            }
            if (stateConfig.cleaner) this.config.hasCleaners = true;
        });



        const KafkaAdmin = require("./kafkaAdmin.js");
        const KafkaProducer = require("./kafkaProducer.js");
        const KafkaConsumer = require("./kafkaConsumer.js");

        this.adminClient = new KafkaAdmin(stateManagerConfig);
        stateManagerConfig.clientId = "consumer";
        this.consumer = new KafkaConsumer(stateManagerConfig);
        stateManagerConfig.clientId = "stateConsumer";
        stateManagerConfig.groupId = null;
        this.stateConsumer = new KafkaConsumer(stateManagerConfig);
        stateManagerConfig.clientId = "stateProducer";
        this.producer = new KafkaProducer(stateManagerConfig);

        const self = this;
        function shutdownConsumer() {
            self.disconnect();
        };
        process.on('SIGINT', shutdownConsumer);

    }

    async connect() {
        await this.adminClient.connect();
        await this.producer.connect();
        await this.consumer.connect();
        this.state.isConnected = true;
        if (this.config.stateTopicConfigs.length > 0) {
            await this.stateConsumer.connect();
            await this.stateConsumer.subscribe(this.config.stateTopicConfigs, true);
            await this.stateConsumer.disconnect();




            //_runCleaner(this);
            /*await new Promise((resolve, reject) => {
                this.stateConsumer.once("consumer.loaded", async () => {
                    this.state.stateLoaded = true;
                    if (this.config.hasCleaners) _runCleaner(this);
                    try {
                        if (this.state.stateLogIntervalSec) {
                            this.state.stateTimer = setInterval(() => {
                                this.state.states.forEach((state, stateName) => {
                                    this.logger.info("State " + stateName + " has " + state.size + " records");
                                    //console.log();
                                })
                            }, this.state.stateLogIntervalSec * 1000);
                        }
                        await this.stateConsumer.disconnect();

                        resolve();
                    } catch (err) {
                        reject(err);
                    }

                });
            });*/
        }

        this.state.stateLoaded = true;
        if (this.config.hasCleaners) _runCleaner(this);
        try {
            if (this.state.stateLogIntervalSec) {
                this.state.stateTimer = setInterval(() => {
                    this.state.states.forEach((state, stateName) => {
                        this.logger.info("State " + stateName + " has " + state.size + " records");
                        //console.log();
                    })
                }, this.state.stateLogIntervalSec * 1000);
            }
        } catch (err) {
            throw (err);
        }
    }

    async disconnect() {
        if (this.state.cleanerTimer) clearTimeout(this.state.cleanerTimer);
        if (this.state.stateTimer) clearTimeout(this.state.stateTimer);
        this.state.stateLoaded = false;
        this.state.isConnected = false;
        await this.adminClient.disconnect();
        await this.stateConsumer.disconnect();
        await this.consumer.disconnect();
        await this.producer.disconnect();
    }

    async subscribe(topicConfigs) {
        //await new Promise(async (resolve, reject) => {
        topicConfigs.forEach((topicConfig) => {
            topicConfig.processingType = "CALLBACK";
            topicConfig.handler = (message, done) => {
                let localDone = done;
                if (topicConfig.mapper) {
                    localDone = async () => {
                        let mappings = await topicConfig.mapper(this.state.states, message);
                        let mergePromises = [];
                        mappings.forEach((mapping) => {
                            if (!mapping.stateRecord) mergePromises.push(this.delete(mapping.stateName, mapping.stateKey));
                            else if (mapping.merge == false) mergePromises.push(this.set(mapping.stateName, mapping.stateKey, mapping.stateRecord));
                            else mergePromises.push(this.merge(mapping.stateName, mapping.stateKey, mapping.stateRecord));
                        });
                        await Promise.all(mergePromises);
                        done();
                    }
                }

                if (topicConfig.stateName) _directMapper(this, topicConfig.stateName, message, localDone);
                else localDone();
            }
        });

        await this.consumer.subscribe(topicConfigs, true);
        //resolve();
        //this.consumer.once("consumer.loaded", () => {
        //if (this.config.hasCleaners) _runCleaner(this);
        this.logger.info("Commiting offsets for state topics (NOT realy)");
        //this.consumer.commit();
        //})
        //resolve()
        //});

    }

    async unsubscribe(topicConfigs) {


    }


    async merge(stateName, stateKey, stateRecord) {
        //logger.silly('Command merge() called for state ' + this.stateName);
        let state = this.state.states.get(stateName);
        let oldRecord = null;
        if (state) {
            oldRecord = state.get(stateKey);
            if (oldRecord) {
                const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;
                stateRecord = deepmerge(oldRecord, stateRecord, { arrayMerge: overwriteMerge });
                //logger.silly("Merged state with old state for key: " + this.stateName + "/" + stateKey);
            }
            await this.update(stateName, stateKey, stateRecord);
        } else this.logger.error("Trying to merge to unknown state: "+stateName);

    }

    async update(stateName, stateKey, stateRecord) {
        //logger.silly('Command update() called ' + stateName);
        let state = this.state.states.get(stateName);
        let oldRecord = null;
        if (state) {
            oldRecord = state.get(stateKey);
            if (oldRecord) {
                let equal = isObjectEqual(oldRecord, stateRecord);
                if (!equal) {
                    await this.set(stateName, stateKey, stateRecord);



                    /*if (state) {
                        logger.silly("Updated state for key: " + this.stateName + "/" + stateKey);
                        this.emit('state.change', "update", this.stateName, stateKey, stateRecord, resolve, reject);
                    } else {
                        logger.silly("Created state for key: " + this.stateName + "/" + stateKey);
                        this.emit('state.change', "create", this.stateName, stateKey, stateRecord, resolve, reject);
                    }*/
                }
                //else logger.silly("No change to state for key: " + stateName + "/" + stateKey);
            } else {
                await this.set(stateName, stateKey, stateRecord);
            }
        } else this.logger.error("Trying to update unknown state: "+stateName);
    }

    async set(stateName, stateKey, stateRecord) {
        if (!stateRecord || (stateRecord == "null") || (stateRecord == "undefined"))
            stateRecord = "";
        let state = this.state.states.get(stateName);
        if (state) {
            if (stateRecord != "") state.set(stateKey, stateRecord);
            else state.delete(stateKey);
            let stateConfig = this.config.stateConfigs.get(stateName);
            if (this.state.stateLoaded) {
                if (stateConfig.changeHandler) {
                    await stateConfig.changeHandler(stateName, stateKey, stateRecord);

                }
                if (stateConfig.stateTopic) {
                    let sendMessage = "";
                    if (stateRecord) sendMessage = JSON.stringify(stateRecord);
                    await this.producer.send(stateConfig.stateTopic, sendMessage, stateKey);
                }
            }
        } else this.logger.error("Trying to set unknown state: "+stateName);
    }


    async delete(stateName, stateKey) {
        if (true) {
            await this.set(stateName, stateKey, null)
        } else throw new Error("StateProducer: Not ready to produce state")
    }

    async deleteState(stateName) {
        let state = this.state.states.get(stateName);
        if (state) {
            let delPromises = [];
            state.forEach((value, key) => {
                delPromises.push(this.delete(stateName, key));
            })
            await Promise.all(delPromises);
        }
    }


    async get(stateName, stateKey) {
        return this.state.states.get(stateName).get(stateKey);
    }

    async getState(stateName) {
        return this.state.states.get(stateName);
    }

    async getStateStore() {
        return this.state.states;
    }


}


async function _runCleaner(context) {
    context.logger.info("State cleaner started");
    let async = require("async");
    await async.whilst(
        async () => {
            //return true;
            return context.state.isConnected;
            //callback(null, true);
            //callback(null, context.state.isConnected); 
        },
        async () => {
            context.logger.info("State cleaner iteration starting");
            await async.eachLimit(context.config.stateManagerConfig.states, 1,
                async (stateConfig) => {
                    if (stateConfig.cleaner) {
                        context.logger.info("Cleaning: " + stateConfig.stateName);
                        let stateRecords = context.state.states.get(stateConfig.stateName)
                        let count = 0;
                        //await new Promise((resolve, reject) => {
                        await async.eachOfLimit(stateRecords, 1,
                            async (data) => {
                                let key = data[0];
                                let value = data[1];

                                let result = await stateConfig.cleaner(stateConfig.stateName, key, value, context.state.states);
                                if (!result) {
                                    //console.log("DELETE: " + key);
                                    await context.delete(stateConfig.stateName, key);
                                    count++;
                                }
                                //count++;
                                await new Promise((resolve, reject) => {
                                    setImmediate(() => {
                                        resolve();
                                    });
                                });
                                /*,
                                function (err, n) {
                                    //callback(null, true);
                                    if (!err) resolve();
                                    else reject();
                                }*/
                            }
                            //})
                            /*await new Promise((resolve, reject) => {
                                setTimeout(()=>{
                                    logger.info("CLEANED: " + stateConfig.stateName );
                                    resolve();
                                }, 5000);
                            })*/
                        );
                        context.logger.info("Cleaned: " + stateConfig.stateName + " (" + count + ")");
                    }
                }
            )
            context.logger.info("Ended state cleaner iteration");
            await new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve();
                }, 60000);
            })
        }
        /*,
        function (err, n) {
            // 5 seconds have passed, n = 5
            context.logger.info("State cleaner stopped")
        }*/
    );
    context.logger.info("State cleaner stopped");
}

async function _stateHandler(context, stateName, message, done) {
    //let stateName = context.state.stateTopics.get(message.topic);
    //let state = context.states.get(stateName);
    if (message.value) {
        await context.set(stateName, message.key, message.value);
    }
    else
        await context.delete(stateName, message.key);
    done();
    //console.log();
}

async function _directMapper(context, stateName, message, done) {
    if (message.value) await context.merge(stateName, message.key, message.value);
    else await context.delete(stateName, message.key);
    done();

}

function isObjectEqual(oldObject, newObject) {
    let changes = diff(oldObject, newObject);
    if (Object.keys(changes).length === 0) return true;
    else return false;
}

module.exports = kafkaStateManager;