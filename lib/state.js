const EventEmitter = require('events');
const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");

const sharedFunctions = require("./shared.js");

const env = process.env.NODE_ENV || 'development';

var logger;

function isObjectEqual(oldObject, newObject) {
    let changes = diff(oldObject, newObject);
    if (Object.keys(changes).length === 0) return true;
    else return false;
}


class state extends EventEmitter {
    constructor(stateName, type) {
        super();
        this.state = new Map();
        this.stateName = stateName;

        logger = sharedFunctions.getLogger(null, "State");

        logger.info('Initializing state: ' + stateName);
    }


    merge(stateKey, stateRecord,resolve,reject) {
        logger.silly('Command merge() called for state ' + this.stateName);
        var state = this.state.get(stateKey);
        if (state) {
            const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;
            stateRecord = deepmerge(state, stateRecord, { arrayMerge: overwriteMerge });
            logger.silly("Merged state with old state for key: " + this.stateName + "/" + stateKey);
        }

        this.update(stateKey, stateRecord,resolve,reject);
    }

    update(stateKey, stateRecord, _oldStateRecord,resolve,reject) {
        logger.silly('Command update() called ' + this.stateName);

        let state;
        if (_oldStateRecord || (_oldStateRecord == null)) state = _oldStateRecord;
        else state = this.state.get(stateKey);
        let equal = false;
        if (state) {
            equal = isObjectEqual(state, stateRecord);
        }
        if (!equal) {
            this.state.set(stateKey, stateRecord);
            if (state) {
                logger.silly("Updated state for key: " + this.stateName + "/" + stateKey);
                this.emit('state.change', "update", this.stateName, stateKey, stateRecord,resolve,reject);
            } else {
                logger.silly("Created state for key: " + this.stateName + "/" + stateKey);
                this.emit('state.change', "create", this.stateName, stateKey, stateRecord,resolve,reject);
            }
        }
        else logger.silly("No change to state for key: " + this.stateName + "/" + stateKey);
    }

    blindSet(stateKey, stateRecord ) {
        logger.silly('Command blindSet() called ' + this.stateName);


        this.state.set(stateKey, stateRecord);

        logger.silly("Updated stated/created for key: " + this.stateName + "/" + stateKey);

    }

    blindDelete(stateKey, stateRecord ) {
        logger.silly('Command blindDelete() called ' + this.stateName);


        this.state.delete(stateKey);

        logger.silly("Deleted key: " + this.stateName + "/" + stateKey);

    }

    delete(stateKey) {
        logger.silly('Command delete() called for state ' + this.stateName);
        var state = this.state.get(stateKey);
        if (state) {
            this.state.delete(stateKey);
            this.emit('state.change', "delete", this.stateName, stateKey, null);
            logger.silly("Deleted state for key: " + this.stateName + "/" + stateKey);
        } else {
            logger.warn("Tried to delete non-existent key: " + this.stateName + "/" + stateKey);
        }
    }

    get(stateKey) {
        logger.silly('Command get() called for state ' + this.stateName);
        var state = this.state.get(stateKey);
        if (state) {
            logger.silly("Got state for key: " + this.stateName + "/" + stateKey);
        } else {
            logger.warn("Tried to get non-existent state with key: " + this.stateName + "/" + stateKey);
        }
        return state;
    }

    getState() {
        logger.debug('Command getState() called for state ' + this.stateName);
        return this.state;
    }

    clearState() {
        logger.debug('Command clearState() called for state ' + this.stateName);

        this.state.forEach((stateRecord, stateKey) => {
            this.delete(stateKey)
        })
    }
}

module.exports = state;