
// High-level wraper for node-rdkafka handeling backpressure, commit, reasignments and message deserialization
// Implemented: 
//        Back preassure, reassign, subscribe, unsubscribe, 
//        Promise based api
//        avro schema registry,
//        message deserializing of json and avro, 
//        logging
//        gracefull shutdown, 
//        async and sync message handling,
//        maxAge filter
//        user filter
//        EOF detection
// To be implemented: 
//        Per topic trottling
//        Automatic commit on time and count
//        

"use strict";

const EventEmitter = require('events');
const kafka = require('node-rdkafka');
const async = require("async");
const clone = require('clone');

//const uuid = require("uuid/v4");
const { v4: uuid } = require('uuid');
const sharedFunctions = require("./shared.js");
const env = process.env.NODE_ENV || 'development';

class KafkaConsumer extends EventEmitter {
    constructor(consumerConfig) {
        super();

        this.consumer = null;
        this.config = {
            consumerConfig: consumerConfig
        };

        this.state = {
            brokerTopics: new Map(),
            subscribedTopics: new Map(),
            subscribedPartitions: new Map(),
            consumerIsPaused: true,
            isAssigned: false
        }

        this.processingQueue = async.priorityQueue((message, done) => {
            _processMessage(this, message, done);
        }, 1);

        consumerConfig.groupId = consumerConfig.groupId || uuid();

        consumerConfig.clientId = consumerConfig.clientId || uuid();
        consumerConfig.queueSize = consumerConfig.queueSize || 2000;
        consumerConfig.schemaRegistryUrl = consumerConfig.schemaRegistryUrl || null;
        consumerConfig.statsLogIntervalSec = consumerConfig.statsLogIntervalSec || null;
        consumerConfig.memoryLogIntervalSec = consumerConfig.memoryLogIntervalSec || null;
        consumerConfig.brokerList = consumerConfig.brokerList || ["localhost:9092"];

        if (!Array.isArray(consumerConfig.brokerList)) consumerConfig.brokerList = [consumerConfig.brokerList];

        const MessageDecoder = new require("./decodeMessage.js")
        this.decoder = new MessageDecoder(consumerConfig.schemaRegistryUrl);


        this.logger = sharedFunctions.getLogger(null, "KafkaConsumer>" + consumerConfig.clientId);
        this.logger.info('Kafka consumer initializing');
        this.logger.info('Using groupID: ' + consumerConfig.groupId);

        this.config.nodeRDKafkaConfig = {
            'group.id': consumerConfig.groupId,
            'metadata.broker.list': consumerConfig.brokerList,
            'rebalance_cb': (err, assignments) => {
                _onRebalance(this, err, assignments);
            },
            "client.id": consumerConfig.clientId,
            //"api.version.request": true,
            "auto.offset.reset": "beginning",
            "socket.keepalive.enable": true,
            "enable.auto.commit": false,
            "message.max.bytes":157286400,
            //"receive_message_max_bytes":157286400,
            "enable.auto.offset.store": false,
            //"broker.version.fallback": "0.10.1",
            "statistics.interval.ms": 1000,
            'offset_commit_cb': (err, assignments) => {
                _onCommit(this, err, assignments);
            }
        };


        this.stats = {
            timestamp: new Date(),
            performance: {
                recievedMsgPerSec: null,
                processedMsgPerSec: null,
                skipedMsgPerSec: null,
                processingLatencyMs: null
            },
            counters: {
                recieved: 0,
                processed: 0,
                skips: 0,
                agrProcessingTime: 0,
            }
        };

        //gracefull shutdown of consumer
        const self = this;
        function shutdownConsumer() {
            self.disconnect();
        };
        process.on('SIGINT', shutdownConsumer);

        this.logger.info('Configuring Kafka broker(s): ' + JSON.stringify(this.config.consumerConfig.brokerList));
        this.consumer = new kafka.KafkaConsumer(this.config.nodeRDKafkaConfig, {
            "auto.offset.reset": "beginning"
        });

        this.consumer.on('event.stats', function (message) {
            //self.logger.info(message.message);
        });

        this.consumer.on('event.event', function (message) {
            self.logger.info(message);
        });
        this.consumer.on('event.error', function (message) {
            self.logger.error(message);
            throw(message);
        });

        this.consumer.on('data', function (message) { _recieveMessage(self, message); });
    }

    async connect() {
        await new Promise((resolve, reject) => {
            this.consumer.connect({}, (err, metadata) => {

                //Handle errors
                if (err) {
                    this.logger.error(err);
                    reject(err);
                } else {

                    _updateBokerTopics(this, metadata.topics);

                    this.consumer.setDefaultConsumeTimeout(1);
                    _consume(this);




                    this.emit('consumer.connected', true);
                    this.logger.info('Connected to brokers: ' + JSON.stringify(this.config.consumerConfig.brokerList));

                    resolve();
                }
            });
        });
    }

    async disconnect() {
        await new Promise((resolve, reject) => {
            if (!this.consumer._isDisconnecting) {
                if (this.consumer.isConnected()) {
                    //this.pause();
                    this.unsubscribe().then(() => {
                        this.consumer.disconnect((err) => {
                            if (err) {
                                this.logger.error(err);
                                reject(err);
                            }
                            _updateBokerTopics(this, []);
                            this.logger.info("Disconnected from Kafka broker(s)");
                            this.emit('consumer.disconnected', true);
                            resolve();
                        });
                    })
                } else {
                    this.logger.warn("All ready disconnected from Kafka broker(s)");
                    resolve();
                }
            } else {
                resolve();
            }
        });
    }

    async subscribe(topics, stage) {



        await new Promise((resolve, reject) => {
            if ((!this.consumer._isDisconnecting) && (this.consumer.isConnected())) {
                if (!Array.isArray(topics)) topics = [topics];
                let additions = false;
                const subscriptions = clone(this.consumer.subscription());
                const hasSubscriptions = subscriptions.length > 0;
                topics.forEach(topicConfig => {
                    let topic = topicConfig.topic;
                    if (this.state.brokerTopics.has(topic)) {
                        if (!subscriptions.includes(topic)) {
                            topicConfig.keepOffsetOnReassign = topicConfig.keepOffsetOnReassign || true;
                            topicConfig.messageDecoder = topicConfig.messageDecoder || "AUTO";
                            topicConfig.startFromLatest = topicConfig.startFromLatest || false;
                            topicConfig.processingType = topicConfig.processingType || "EVENT";
                            topicConfig.priority = topicConfig.priority || 10000000;
                            topicConfig.partitions = new Map();
                            this.state.subscribedTopics.set(topic, topicConfig);
                            subscriptions.push(topic);
                            additions = true;
                            this.logger.info("Subscribing to: " + topic);
                            this.emit('subscription.subscribed', topic);
                            this.emit('subscription.subscribed.' + topic, true);
                        } else this.logger.warn("Topic is all ready subscribed: " + topic);
                    } else this.logger.warn("Not subscribing. Topic does not exist on broker: " + topic);
                });
                if (additions) {
                    if (hasSubscriptions) this.consumer.unsubscribe();
                    this.consumer.subscribe(subscriptions);
                }

                if (stage) {
                    this.once("consumer.loaded", () => resolve(subscriptions));

                } else resolve(subscriptions);
            } else {
                this.logger.error("Not subscribing. Consumer is not connected.");
                reject();
            }
        });
    }

    async unsubscribe(topics) {
        await new Promise((resolve, reject) => {
            if ((!this.consumer._isDisconnecting) && (this.consumer.isConnected())) {
                const newSubscription = [];
                if (!topics) {
                    if (this.consumer.subscription().length > 0) {
                        topics = this.consumer.subscription();
                        this.logger.info("Unsubscribing from all topics");
                    } else this.logger.warn("Unsubscribe from all topics when no topic is subscribed");
                }
                if (!Array.isArray(topics)) topics = [topics];
                let subtractions = false;
                const subscriptions = clone(this.consumer.subscription());
                topics.forEach(topic => {
                    if (!subscriptions.includes(topic)) {
                        this.logger.warn("Not unsubscribing. Topic is not subscribed: " + topic);
                    }
                });
                subscriptions.forEach(topic => {
                    if (!topics.includes(topic)) {
                        newSubscription.push(topic);
                    } else {
                        this.state.subscribedTopics.delete(topic);
                        this.logger.info("Unsubscribing from topic: " + topic);
                        this.emit('subscription.unsubscribed', topic);
                        this.emit('subscription.unsubscribed.' + topic, true);
                        subtractions = true;
                    }
                });
                if (subtractions) {
                    this.consumer.unsubscribe();
                    if (newSubscription.length > 0) {
                        this.consumer.subscribe(newSubscription);
                    }
                }
                resolve(newSubscription);
            } else {
                this.logger.warn("Not unsubscribing. Consumer is not connected.");
                resolve();
            }
        });
    }

    async commit(topic, partition, offset) {
        await new Promise((resolve, reject) => {
            if ((!this.consumer._isDisconnecting) && (this.consumer.isConnected())) {
                if (topic && (partition || (partition === 0)) && offset) this.consumer.commit({ topic: topic, partition: partition, offset: offset });
                else this.consumer.commit();
                resolve();
            } else {
                this.logger.error("Not committing. Consumer is not connected.");
                reject();
            }
        });
    }

    async pause() {
        this.processingQueue.pause();
        this.emit('consumer.paused', true);
        this.logger.info("Paused consumer");

    }

    async resume() {
        this.processingQueue.resume();
        this.emit('consumer.resumed', true);
        this.logger.info("Resumed consumer");
    }
}

function _onCommit(context, err, assignments) {
    if (err) {
        // There was an error committing
        console.error(err);
    } else {
        // Commit went through. Let's log the topic partitions
        assignments.forEach(assignment => {
            const topicState = context.state.subscribedTopics.get(assignment.topic);
            if (topicState) {
                const partitionState = topicState.partitions.get(assignment.partition);

                partitionState.lastProcessedOffset = assignment.offset;
                if (partitionState.offsetResolve) partitionState.offsetResolve();
                partitionState.offsetResolve = null;
                //console.log(topicPartitions);
            }
        })

    }

}

// Function to consume messages as quickly as possible but handling backpreassure and trottling
async function _consume(context) {



    // Function to get a batch of messages
    function getMessages(callback) {
        if (context.consumer.isConnected() && !context.consumer._isDisconnecting) {

            // Determine number of messages to request 
            let numberOfMessages = context.config.consumerConfig.queueSize - context.processingQueue.length()

            if (context.state.subscribedTopics.size > 0) // Only consume messages if subscribed
                if (numberOfMessages > 0) // Only consume mesages if there is room in the queue
                    context.consumer.consume(numberOfMessages, (err, data) => {
                        if (err) callback(err);
                        else callback(null, true); // Start next consume
                    });
                else setTimeout(() => { callback(null, true) }, 100); //Delay next consume 100 ms if no room in queue to minimize resource usage
            else setTimeout(() => { callback(null, true) }, 1000); //Delay next consume second if nothing is subscribed to minimize resource usage
        } else callback(null, false); //Stop consuming if not connected
    }

    context.logger.info("Consumer started");

    // Keeps calling getMessages as long as consumer is connected
    await async.doWhilst(getMessages, (result, cb) => {
        cb(null, result);
    });

    context.logger.info("Consumer stopped");
}

function _recieveMessage(context, message) {
    message.recievedTs = sharedFunctions.now('nano');
    context.stats.counters.recieved++;
    if (!context.stats.loadStartTimestamp) {
        context.stats.loadStartTimestamp = new Date();
        context.stats.loadCount = 0;
    }
    context.stats.loadCount++;
    const topic = context.state.subscribedTopics.get(message.topic);
    if (topic) {

        const partition = topic.partitions.get(message.partition);


        if (partition.seekType) {
            if ((partition.seekType == "LATEST") && (message.offset >= partition.seekOffset)) {
                context.logger.info("Starting from LATEST offset of " + message.topic + "/" + message.partition + " on offset: " + message.offset);
                partition.isDirty = false;
                partition.seekOffset = null;
                partition.seekType = null;

            } else if ((partition.seekType == "REASSIGN") && (message.offset >= partition.lastProcessedOffset)) {
                context.logger.info("Starting from lastProcessedOffset+1 after reassign of " + message.topic + "/" + message.partition + " on offset: " + message.offset);
                partition.isDirty = false;
                partition.seekOffset = null;
                partition.seekType = null;

            } else if ((partition.seekType == "MAXAGE") && (!sharedFunctions.isOlderThanMs(message.timestamp, topic.maxAgeSec * 1000))) {
                context.logger.info("Starting from offset younger than maxAgeSec " + message.topic + "/" + message.partition + " on offset: " + message.offset);
                partition.isDirty = false;
                partition.seekOffset = null;
                partition.seekType = null;
            }
        }
        //Push to queue if partition is not dirty
        if (!partition.isDirty) { //isDirty is set when topic partitions are revoked and is not cleared unless previously processed message on that topic partition is reached on later assignment
            if (message.offset > partition.lastRecievedOffset) { //Ensure that when startFrom="BEGINNING" only messages are only processed once (occasional timing glitch)
                context.processingQueue.push(message, topic.priority, (err) => {
                    partition.lastRecievedOffset = message.offset;
                });
            } else context.stats.counters.skips++;
        } else context.stats.counters.skips++;

    } else context.stats.counters.skips++;
}


function _updateBokerTopics(context, topics) {
    context.state.brokerTopics.clear();
    if (Array.isArray(topics)) {
        topics.forEach(topic => {
            context.state.brokerTopics.set(topic.name, topic);
        })
    }
}

function _onRebalance(context, err, assignments) {
    if (err.code === kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {

        if (!context.consumer._isDisconnecting && context.consumer.isConnected() && (assignments.length > 0)) {
            const assigmentPromises = [];

            assignments.forEach(assignment => {
                assigmentPromises.push(new Promise((resolve, reject) => {
                    context.consumer.queryWatermarkOffsets(assignment.topic, assignment.partition, 5000, (err, offsets) => {
                        let highOffset = -1;
                        let lowOffset = -1
                        if (!err) {
                            highOffset = offsets.highOffset;
                            lowOffset = offsets.lowOffset;

                        } else reject();

                        const topicState = context.state.subscribedTopics.get(assignment.topic);
                        if (!topicState.partitions.has(assignment.partition)) {
                            topicState.partitions.set(assignment.partition, {
                                highOffset: -1,
                                lowOffset: -1,
                                lastRecievedOffset: -1,
                                lastProcessedOffset: -1,
                                lastCommitedOffset: -1,
                                isDirty: false,
                                isAssigned: false,
                                isLoaded: false,
                                justAssigned: false
                            })
                        }
                        const partitionState = topicState.partitions.get(assignment.partition);
                        partitionState.isLoaded = false;
                        topicState.isLoaded = false;
                        context.state.isLoaded = false;
                        context.state.loadStartTimestamp = null;
                        context.state.loadCount = 0;

                        partitionState.isAssigned = true;
                        partitionState.justAssigned = true;
                        partitionState.highOffset = highOffset;
                        partitionState.lowOffset = lowOffset

                        if (topicState.maxAgeSec) {
                            partitionState.isDirty = true;
                            partitionState.seekType = "MAXAGE";
                            partitionState.seekOffset = null;
                        }

                        context.logger.info("Consumer assignment added: " + assignment.topic + " partition " + assignment.partition);
                        resolve();
                    });
                }));
            });
            Promise.all(assigmentPromises).then(async () => {
                //context.consumer.assign(assignments);
                //await new Promise((resolve, reject) => {
                context.consumer.assign(assignments);
                //setTimeout(() => { // Seek needs to be delayed due to async issues with node-rdkafka and assign
                //    resolve();
                //}, 100);
                //});

                let seekPromises = [];
                assignments.forEach((assignment) => {
                    seekPromises.push(new Promise((resolve, reject) => {
                        const topicState = context.state.subscribedTopics.get(assignment.topic);
                        const partitionState = topicState.partitions.get(assignment.partition);
                        if ((partitionState.lastProcessedOffset >= 0) && (topicState.keepOffsetOnReassign)) {
                            partitionState.isDirty = true;
                            partitionState.seekType = "REASSIGN";
                            setTimeout(() => { // Seek needs to be delayed due to async issues with node-rdkafka and assign
                                context.consumer.seek({ topic: assignment.topic, partition: assignment.partition, offset: partitionState.lastProcessedOffset-1 }, 10000, (err) => {
                                    if (err)
                                        reject();
                                    else resolve();
                                });
                            }, 100);
                        } else if (topicState.startFromLatest) {
                            partitionState.isDirty = true;
                            partitionState.seekType = "LATEST";
                            partitionState.seekOffset = partitionState.highOffset - 1;
                            setTimeout(() => { // Seek needs to be delayed due to async issues with node-rdkafka and assign
                                context.consumer.seek({ topic: assignment.topic, partition: assignment.partition, offset: "end" }, 5000, (err) => {
                                    if (err)
                                        reject();
                                    else resolve();
                                });
                            }, 100);
                        } else {
                            partitionState.lastProcessedOffset = -1;
                            partitionState.lastRecievedOffset = -1;
                            resolve();
                        }
                    }));
                });
                Promise.all(seekPromises).then(() => {
                    context.consumer.resume(assignments);
                    context.state.isAssigned = true;
                    _startLogging(context);
                });
            });
        } else context.consumer.assign(assignments);
    } else if (err.code === kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {

        //Stop stats logger
        _stopLogging(context);
        context.state.isAssigned = false;
        context.processingQueue.remove(() => { return true });
        context.consumer.unassign();
        assignments.forEach(assignment => {
            const topicState = context.state.subscribedTopics.get(assignment.topic);
            if (topicState) { //If unsubscribing there is no topic or partition settings at this point.
                const partitionState = topicState.partitions.get(assignment.partition);
                //partitionState.isDirty = true;
                partitionState.seekOffset = null;
                partitionState.seekReason = null;
                context.logger.info("Consumer assignment removed: " + assignment.topic + "/" + assignment.partition + " LastProcessedOffset: " + partitionState.lastProcessedOffset);
            } else context.logger.info("Consumer assignment removed: " + assignment.topic + "/" + assignment.partition + " (Unsubscribed)");
        });
    } else {
        context.logger.error(err);
        context.disconnect();
        throw (err);
    }
}

async function _queryWatermarkOffsets(context, topic, partition) {
    return await new Promise((resolve, reject) => {
        context.consumer.queryWatermarkOffsets(topic, partition, timeout, function (err, offsets) {
            if (err) reject(err);
            else resolve(offsets);
        });
    });
}

function _startLogging(context) {
    if ((!context.statsTimer) && (context.config.consumerConfig.statsLogIntervalSec)) {

        context.statsTimer = setInterval((context) => {
            const diff = new Date() - context.stats.timestamp;
            context.stats.timestamp = new Date();
            context.stats.performance.recievedMsgPerSec = Math.round(context.stats.counters.recieved / (diff / 1000));
            context.stats.performance.processedMsgPerSec = Math.round(context.stats.counters.processed / (diff / 1000));
            context.stats.performance.skipedMsgPerSec = Math.round(context.stats.counters.skips / (diff / 1000));
            context.stats.performance.processingLatencyMs = (context.stats.counters.agrProcessingTime / context.stats.counters.processed) / 1000000;

            context.stats.counters.agrProcessingTime = 0;
            context.logger.info(
                'Recieved: ' + context.stats.performance.recievedMsgPerSec + " msg/s"
                + ', Processed: ' + context.stats.performance.processedMsgPerSec + " msg/s"
                + ', Skipped: ' + context.stats.performance.skipedMsgPerSec + " msg/s"
                + ', Average processing latency: ' + context.stats.performance.processingLatencyMs.toFixed(1) + " ms"
            )
            /*context.logger.info(
                'Recieved: ' + context.stats.counters.recieved + " msg(s)"
                + ', Processed: ' + context.stats.counters.processed + " msg(s)"
                + ', Skipped: ' + context.stats.counters.skips + " msg(s)"
            )*/
            context.stats.counters.recieved = 0;
            context.stats.counters.skips = 0;
            context.stats.counters.processed = 0;
            context.stats.counters.agrProcessingTime = 0;
        }, context.config.consumerConfig.statsLogIntervalSec * 1000, context);
    }

    if ((!context.memorySampleTimer) && (context.config.consumerConfig.memoryLogIntervalSec)) {
        context.stats.agrHeapUsed = 0;
        context.stats.countHeapUsed = 0;
        context.stats.agrExternalUsed = 0;
        context.stats.countExternalUsed = 0;
        context.memorySampleTimer = setInterval((context) => {
            context.stats.agrHeapUsed = context.stats.agrHeapUsed + process.memoryUsage().heapUsed;
            context.stats.countHeapUsed++;
            context.stats.agrExternalUsed = context.stats.agrExternalUsed + process.memoryUsage().external;
            context.stats.countExternalUsed++;
        }, 10, context);
    }

    if ((!context.memoryLogTimer) && (context.config.consumerConfig.memoryLogIntervalSec)) {
        context.memoryLogTimer = setInterval((context) => {
            let heapUsed = (context.stats.agrHeapUsed / context.stats.countHeapUsed) / 1024 / 1024;
            let externalUsed = (context.stats.agrExternalUsed / context.stats.countExternalUsed) / 1024 / 1024;
            context.logger.info(
                "Total heap usage: " + heapUsed.toFixed(3) + " Mb" +
                " / Librdkafka mem. usage: " + externalUsed.toFixed(3) + " Mb"
            );
            context.stats.agrHeapUsed = 0;
            context.stats.countHeapUsed = 0;
            context.stats.agrExternalUsed = 0;
            context.stats.countExternalUsed = 0;
        }, context.config.consumerConfig.memoryLogIntervalSec * 1000, context);
    }
}

function _stopLogging(context) {
    if (context.statsTimer) {
        clearInterval(context.statsTimer);
        context.statsTimer = null;
    }

    if (context.memoryLogTimer) {
        clearInterval(context.memoryLogTimer);
        context.memoryLogTimer = null;
    }

    if (context.memorySampleTimer) {
        clearInterval(context.memorySampleTimer);
        context.memorySampleTimer = null;
    }

}

function _checkIfCommit(context, message) {
    if (!context.state.commitCount) context.state.commitCount = 0;
    context.state.commitCount++
    return false
}

function _processMessage(context, message, queueCallback) { // Not using async function with async package. Has unidentified performance problem.
    const topic = context.state.subscribedTopics.get(message.topic);
    if (topic) {
        const partition = topic.partitions.get(message.partition);

        context.decoder.decodeMessage(message, topic.messageDecoder).then((decodedMessage) => {
            let process = true;
            if (topic.filter) process = topic.filter(message);
            if (process) {

                function callback() {
                    const now = sharedFunctions.now('nano');
                    partition.lastProcessedOffset = decodedMessage.offset;
                    context.stats.counters.processed++;
                    context.stats.counters.agrProcessingTime = context.stats.counters.agrProcessingTime + (now - message.recievedTs);


                    //Check if loaded
                    if (!context.state.isLoaded && context.state.isAssigned) {
                        const watermarks = context.consumer.getWatermarkOffsets(message.topic, message.partition);
                        partition.highOffset = watermarks.highOffset;
                        partition.lowOffset = watermarks.lowOffset;
                        if (!topic.isLoaded)
                            if (!partition.isLoaded)
                                if (message.offset == watermarks.highOffset - 1) {
                                    partition.isLoaded = true;
                                    context.logger.info("Partition loaded to latest " + message.topic + "/" + message.partition + " on offset: " + message.offset);
                                    let topicLoaded = true;
                                    topic.partitions.forEach(partition => {
                                        if (!partition.isLoaded) topicLoaded = false;
                                    })
                                    if (topicLoaded) {
                                        topic.isLoaded = true;
                                        context.emit("consumer.loaded." + topic.topic, true)
                                        context.logger.info("Topic loaded to latest " + message.topic);
                                        let loaded = true;
                                        context.state.subscribedTopics.forEach(topic => {
                                            if (!topic.isLoaded) loaded = false;
                                        })
                                        if (loaded) {
                                            context.state.isLoaded = true;
                                            context.emit("consumer.loaded", true)
                                            let diff = (new Date() - context.stats.loadStartTimestamp) / 1000;
                                            let msgSpeed = context.stats.loadCount / diff;
                                            context.logger.info("Consumer loaded to latest. " + context.stats.loadCount + " messages recieved in " + diff.toFixed(0) + " seconds (" + msgSpeed.toFixed(0) + " msg/s)");
                                        }
                                    }
                                }
                    }
                    /*if (_checkIfCommit(context, decodedMessage))
                        context.commit(decodedMessage.topic, decodedMessage.partition, decodedMessage.offset).then(() => {
                            context.state.commitCount = 0;
                            queueCallback(null);
                        })
                    else*/
                    queueCallback(null);
                }
                if (topic.handler) {
                    if (topic.processingType == "CALLBACK") topic.handler(decodedMessage, callback);
                    else if (topic.processingType == "ASYNC") topic.handler(decodedMessage).then(() => { callback() });
                    else { // SYNC, Syncronous handling (This will block the consumer....)
                        topic.handler(decodedMessage);
                        callback();
                    }
                } else { // Fallback to EVENT (That's no backpreassure handling....)
                    context.emit("message", decodedMessage);
                    callback();
                }
            } else {
                partition.lastProcessedOffset = decodedMessage.offset;
                context.stats.counters.skips++;
                queueCallback(null);
            }
        });
    }
}

module.exports = KafkaConsumer;