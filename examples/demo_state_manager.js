const Kafka = require("../lib/kafkaConnector.js");

const stateManager = new Kafka.StateManager(
    {
        groupId: "TestDemoTest2",
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
        schemaRegistryUrl: "http://10.10.0.13:8081",
        queueSize: 5000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
        maxWaitMs: 100,
        statsLogIntervalSec: 10,
        memoryLogIntervalSec: 60,
        stateLogIntervalSec: 20,
        states: [
            //stateTopic: "NG.entity.actualcall.key"
            { stateName: "actualCall", cleaner: actuallCallCleaner, changeHandler: null },
            //{ stateName: "vehicle", stateTopic: "e.vehicle.key", changeHandler: null },
            { stateName: "quay", stateTopic: "NG.entity.quay.key", changeHandler: null },
            //{ stateName: "stopPlace", stateTopic: "NG.entity.stopplace.key", cleaner: null, changeHandler: null },
            //{ stateName: "line", stateTopic: "NG.entity.line.key", cleaner: null, changeHandler: null },
        ]
    }
)

function _isOlderThan(date, age) {
    const now = new Date();
    const then = new Date(date);
    const diff = now - date;
    if (diff > age) return true;
    else return false;
}

async function actuallCallCleaner(stateName, stateKey, stateRecord, state) {
    let result = true;
    const now = new Date();
    const then = new Date(stateRecord.timestamp);
    const diff = now - then;
    if ((stateRecord.Status == "DONE") && (diff > (60000 * 1)))
        result = false;
    else if ((stateRecord.Status == "ARRIVED") && (stateRecord.IsLastInJourney) && (diff > (60000 * 1)))
        result = false;
    else if (diff > (60000 * 60 * 12))
        result = false;


    return result;
    //console.log(stateKey);
}



async function actuallCallHandler(stateName, stateKey, stateRecord) {
    //console.log(stateKey);
}


async function run() {
    await stateManager.connect();

    console.log("State loaded");
    /*for(var i=0;i<1000;i++){
        await stateManager.set("testState",i.toString(),{id:i,newTs:new Date()});
    }
    //await stateManager.merge("testState","123",{id:123,newTs:new Date()});*/
    //await stateManager.deleteState("actualCall");
    //console.log("State deleted");

    await stateManager.subscribe([
        {
            topic: "STAGE.entity.actualcall.status",
            mapper: actualCallMapper,
            //stateName: "testState",
            keepOffsetOnReassign: true,
            priority: 2,
            maxAgeSec: 3600*24,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            //handler: callbackHandler,
            //processingType: "CALLBACK",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualcall.estimatedtime",
            mapper: actualCallMapper,
            //stateName: "testState",
            keepOffsetOnReassign: true,
            priority: 1,
            maxAgeSec: 3600*24,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            //handler: asyncHandler,
            //processingType: "ASYNC",
            commitMode:"LOADED",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualcall.key.v2",
            mapper: actualCallMapper,
            //stateName: "testState",
            keepOffsetOnReassign: true,
            priority: 0,
            maxAgeSec: 3600*24,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            //handler: syncHandler,
            //processingType: "SYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.pass.key",
            mapper: passMapper,
            keepOffsetOnReassign: true,
            priority: 3,
            maxAgeSec: 3600*24,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            //handler: syncHandler,
            //processingType: "SYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        /*{
             topic: "STAGE.batch.journey.dated-vehicle-journey-stops",
             mapper: datedJourneyMapper,
             keepOffsetOnReassign: true,
             priority: 0,
             //maxAgeSec: 3600,
             //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
             //handler: syncHandler,
             //processingType: "SYNC",
             startFromLatest: false,    // Starts from latest message on connect
             messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
         }*/
    ]);
    console.log("topics subscribed");
    //await stateManager.disconnect();
}

async function actualCallMapper(states, message) {
    let result = [];
    if (message)
        if (message.value)
            if (message.value.EntityPartition)
                if (message.value.EntityPartition.ActualCallRef != "") {
                    let actualCallState = {
                        stateName: "actualCall",
                        stateKey: message.value.EntityPartition.Key,
                        stateRecord: message.value.EntityPartition,
                        merge: true
                    }
                    actualCallState.stateRecord.timestamp = message.value.Timestamp;
                    result.push(actualCallState);
                }
                
    return result;
}


async function passMapper(states, message) {
    let result = [];
    if (message.value.EntityPartition.ActualCallRef != "") {
        let actualCallState = {
            stateName: "actualCall",
            stateKey: message.value.EntityPartition.ActualCallRef,
            merge: true,
            stateRecord: {
                actualCallRef: message.value.EntityPartition.ActualCallRef,

                passTimestamp: message.value.Timestamp,

                entrypoint: message.value.EntityPartition.Entrypoint,
                exitPoint: message.value.EntityPartition.ExitPoint,
                passStatus: message.value.EntityPartition.Status
            }
        }
        actualCallState.stateRecord.timestamp = message.value.Timestamp;
        result.push(actualCallState);
    }

    return result;
}

async function datedJourneyMapper(states, message) {
    let result = [];
    if (message.value) {
        let lineType = "NORMAL";
        if (message.value.linePublicCode.includes("N")) lineType = "NIGHT";
        else if (message.value.linePublicCode.includes("E")) lineType = "EXPRESS";
        else if (message.value.linePublicCode.includes("X")) lineType = "EXTRA";
        let line = {
            stateName: "line",
            stateKey: message.value.lineId,
            stateRecord: {
                lineRef: message.value.lineId,
                publicCode: message.value.linePublicCode,
                lineType: lineType
            }
        }
        result.push(line);

        message.value.quayStops.forEach(quay => {
            let isDepot = false;
            if (quay.quayId.substring(0, 3) == "RUT") isDepot = true;
            if (quay.publicCode == "") quay.publicCode = null;
            if (quay.description == "") quay.description = null;


            let _quay = {
                stateName: "quay",
                stateKey: quay.quayId,
                stateRecord: {
                    quayRef: quay.quayId,
                    stopPlaceRef: quay.stopId,
                    name: quay.stopName,
                    location: [quay.position.lon, quay.position.lat],
                    privateCode: quay.quayOldId,
                    platform: quay.publicCode,
                    isDepot: isDepot,
                    description: quay.description
                }
            }
            result.push(_quay);

            let stopId = quay.stopId;
            let name = quay.stopName;
            let quayRefs;
            let merge = true;
            let oldRecord = states.get("stopPlace").get(quay.stopId);
            if (oldRecord) {
                if (oldRecord.quayRefs) {
                    quayRefs = oldRecord.quayRefs;
                    if (!oldRecord.quayRefs.includes(quay.quayId)) {
                        quayRefs.push(quay.quayId);
                        merge = false;
                    }
                } else quayRefs = [quay.quayId];
            } else {
                quayRefs = [quay.quayId];
            }
            stopPlace = {
                stateName: "stopPlace",
                stateKey: stopId,
                stateRecord: {
                    stopPlaceRef: quay.stopId,
                    name: name,
                    isDepot: isDepot,
                    quayRefs: quayRefs
                },
                merge: merge
            }

            result.push(stopPlace);
        });
    }

    return result;
}



run();
