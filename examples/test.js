const KafkaConsumer = require("../lib/kafkaConsumer");

const consumer = new KafkaConsumer(
    {
        groupId: "test69",
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
        schemaRegistryUrl: "http://10.10.0.13:8081",
        queueSize: 3000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
        statsLogIntervalSec: 2,
        memoryLogIntervalSec: 30
    }
)


async function run() {
    //Connect to consumer
    await consumer.connect();

    consumer.on("connection.disconnected", () => {
        console.log("DEMO: Consumer disconnected.")
    });
    //Subscribe to topics
    await consumer.subscribe([
       /* {
            topic: "NG.entity.actualcall.key",        //Topic to subscribe to
            keepOffsetOnReassign: true, //Keep processing from last processed offset on reassign to previously assigned topic/partition regardless of commits.
            priority: 2,                //Procesing queue priority, lower numbers process first.
            //maxAgeSec: 3600,          //Drop (do not process) messages older than maxAgeSec
            //filter: randomFilter,       // Filter function to filter out messages from processing called with filter(message). Return false to drop message.
            handler: callbackHandler,   // Function to process message. Call: handler(message,[callback]) Callback is only used if processingtype is CALLBACK
            processingType: "CALLBACK", //Processing type to handle backpreasure. CALLBACK: Call callback function when done, SYNC: Lock until call returns, ASYNC: When promise is resolved.
            startFromLatest: false,     // Always starts from latest message on connect and not beginning or last commit if true
            messageDecoder: "JSON",   // AUTO,JSON,AVRO,STRING,BINARY (Auto automaticaly detects and decodes JSON,AVRO and Text)
        },*/
        {
            topic: "NG.entity.actualcall.key",
            keepOffsetOnReassign: false,
            priority: 1,
            //maxAgeSec: 3600,
            handler: asyncHandler,
            processingType: "ASYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "AUTO", // AUTO,JSON,AVRO,STRING,BINARY
        },
        /*{
            topic: "test_avro",
            keepOffsetOnReassign: true,
            priority: 0,
            filter: randomFilter,
            handler: syncHandler,
            processingType: "SYNC",
            startFromLatest: true,    // Starts from latest message on connect
            messageDecoder: "AVRO", // AUTO,JSON,AVRO,STRING,BINARY (Auto automaticaly detects and decodes JSON,AVRO and Text)
        }*/
    ]);
}

function randomFilter(message) {
    // Implement a filter
    return Math.round(Math.random());
}

async function callbackHandler(message, done) {

    // Do some stuff
//console.log(message);

    // Call callback
    done();
}


async function asyncHandler(message) {
    await new Promise((resolve, reject) => {

        // Do some stuff
        //console.log(message);
        
        //Resolve promise
        resolve();
    });
}


function syncHandler(message) {

    // Do some stuff
    console.log(message);

    // Return to caller
    return;
}

run();
