const Kafka=require("../lib/kafkaConnector.js");

const admin = new Kafka.Admin(
    {
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
    }
)

async function run() {
    // createTopic(topic, numberOfpartitions (default:1), replicationFactor (default:1), config (object with topic properties))
    await admin.connect();
    await admin.deleteTopic("test_plain");
    await admin.deleteTopic("test_json");
    await admin.deleteTopic("test_avro");
    await admin.createTopic("test_plain");
    await admin.createTopic("test_json");
    await admin.createTopic("test_avro");
    //await admin.createPartitions("test", 3);
    
    
}

run();
