const KafkaProducer = require("../lib/kafkaProducer");

//Create producer
const producer = new KafkaProducer(
    {
        clientId: "10.10.0.13", 
        brokerList: ["server-docker-worker-01:9092"],
        //schemaRegistryUrl: ["http://10.10.0.13:8081"],
        queueSize: 3000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
        maxWaitMs: 100,
        //statsLogIntervalSec: 2,
        //memoryLogIntervalSec: 30
    }
)


async function run() {
    //Connect to producer
    await producer.connect();
   /* for (let i = 0; i < 1; i++) {

        // Parameters producer.send(topic,message,key,[schema])

        //Send messages to topic with avro Schema (Schema is automaticaly registered/validated against schemaregister)
        await producer.send("test_avro", { Name: "Henrik", Age: 51 }, "1", {
            "type": "record",
            "namespace": "Test",
            "name": "Person",
            "fields": [
                { "name": "Name", "type": "string" },
                { "name": "Age", "type": "int" },
                { "name": "length", "type": "int", default: -1 }
            ]
        });

        //Send messages to topic with plain text
        

        //Send messages to topic as json from object
        //await producer.send("test_json", JSON.stringify({ msg: "Hei", date: "2020-02-02T11:22:33.232Z" }), "1");
    }*/
    await producer.send("test", "123", "1");
    await producer.disconnect();
}

run();
