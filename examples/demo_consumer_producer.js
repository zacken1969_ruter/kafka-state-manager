//const KafkaProducer = require("../lib/kafkaProducer");
const Kafka=require("../lib/kafkaConnector.js");

const producer = new Kafka.Producer(
    {
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
        schemaRegistryUrl: "http://10.10.0.13:8081",
        queueSize: 2000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
        maxWaitMs:10,
        //statsLogIntervalSec: 2,
        //memoryLogIntervalSec: 30
    }
)

//const KafkaConsumer = require("./lib/kafkaConsumer");

const consumer = new Kafka.Consumer(
    {
        groupId: "test69",
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
        schemaRegistryUrl: "10.10.0.13:8081",
        queueSize: 2000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
        statsLogIntervalSec: 2,
        memoryLogIntervalSec: 30
    }
)

//const KafkaAdmin= require("./lib/kafkaAdmin");

const admin = new Kafka.Admin(
    {
        clientId: "10.10.0.13",
        brokerList: ["10.10.0.13:9092"],
    }
)

async function run() {
    await producer.connect();
    await consumer.connect();
    await admin.connect();
    await admin.deleteTopic("test");
    await admin.createTopic("test");
    //await admin.createPartitions("test", 3);
    

    await consumer.subscribe([
        {
            topic: "STAGE.entity.actualcall.status",
            keepOffsetOnReassign: true,
            priority: 0,
            //maxAgeSec: 3600,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            handler: asyncHandler,
            processingType: "ASYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualcall.estimatedtime",
            keepOffsetOnReassign: true,
            priority: 0,
            //maxAgeSec: 3600,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            handler: asyncHandler,
            processingType: "ASYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "entity.vehicle.position",
            keepOffsetOnReassign: true,
            priority: 0,
            //maxAgeSec: 3600,
            //filter: randomFilter,        // Filter function to filter out messages from processing filter(message). Return false to drop message.
            handler: asyncHandler,
            processingType: "ASYNC",
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        }
    ]);
    
}

async function asyncHandler(message) {
    await producer.send("test", JSON.stringify(message.value), message.key);
}

run();
